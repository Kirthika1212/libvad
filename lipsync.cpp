/*****************************************************************************
 * Copyright (C) 2016 MulticoreWare Inc
 * Project : LipSync
 *****************************************************************************/

/*
 * This file defines LipSync class.
 */

#ifdef LS_DEBUG
#include <iostream>
#include <fstream>
#include <algorithm>
#endif

#if _WIN32
#include <Windows.h>
#endif /* _WIN32 */

#include <vector>
#include <string>
#include <cstdlib> // free, realloc
#include <cstring> // memcpy
#include <cassert>
#include <sys/stat.h>

#include "lipsync.hpp"
#include "stream.hpp"
#include "support.hpp"
#include "utility.hpp"
#include "ls_threads.hpp"

/* Cpp includes */
#include "ls_backend_types.hpp"
#include "speech_detect.hpp"
#include "face_detect.hpp"
#include "track_face.hpp"
#include "lip_movement.hpp"
#include "dar_lipmovement.hpp"
#include "lip_detect.hpp"
#include "histogram_c_port.hpp"
#include "statistical_analysis.hpp"
#include "convert_YUV_to_RGB.hpp"

/* Debug utils */
#include "utils.hpp"
#include "video.hpp"
#include "ls_file.hpp"

/* Disk Access Removal includes */
#include "generate_lip_input.hpp"
#include "generate_lipmvt_input.hpp"

/* Test functions */
#include "dar_speech_test.hpp"
#include "dar_lipmovement_test.hpp"
#include "dar_correlate_test.hpp"

#define REMOVE_DISK_ACCESS_FACE 1
#define REMOVE_DISK_ACCESS_TRACKFACE 1

#if REMOVE_DISK_ACCESS_FACE

/* Remove Disk file access utils */
#include "generate_input_for_face.hpp"
#include "string_manipulations.hpp" // SplitString
#include "defs.h" // CHECKED_MALLOC
/* Store Frames in cv::Mat */
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#endif // REMOVE_DISK_ACCESS_FACE

#if REMOVE_DISK_ACCESS_TRACKFACE

/* Remove Disk file access utils */
#include "generate_input_for_trackface.hpp"
#include "string_manipulations.hpp" // SplitString
#include "defs.h" // CHECKED_MALLOC
/* Store Frames in cv::Mat */
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#endif // REMOVE_DISK_ACCESS_TRACKFACE

#include "netlicensing_ls_manipulations.hpp" /* Add License support */

/* ML-SDK */
#include "ml.h"

/* Maximum number of callback threads that can co-exist */
#define MAX_CALLBACK_THREADS 1000

/* Number of seconds to buffer before running Sync detections. */
#define CRITICAL_VOLUME 25

/* Folder linits */
#define FOLDER_NUMBER 1000

#define MINUTE_COST 5

/* The threshold in #frames upto which we consider the media as InSync
 * Round off fps to the nearest whole number.
 */
#define CALC_INSYNC_BOUNDARY(fps) ((1.0 / 6.0) * (int(fps + 0.5)))

#define DEFAULT_CLASSIFIERS_PATH "./classifiers/"

#ifdef LS_DEBUG
#define ERR_PRINT(s) std::cerr<<"Error - "<<s<<" "<<__FILE__<<" : "<<__LINE__<<std::endl;
#else
#define ERR_PRINT(s)
#endif

#if LS_DEBUG
#define DEBUG_INFO(...) {printf(__VA_ARGS__); fflush(stdout);}
#else
#define DEBUG_INFO(...) ;
#endif
#include <fvad.h>
/*
 * Packs all callback invocation data.
 */
typedef struct CallbackPack {
  /* Callback function pointer */
  void (*sync_callback) (SynchronizationEvent, void *);
  /* User supplied callback argument */
  void *callback_user_arg;
  /* Synchronization event associated with the callback */
  SynchronizationEvent *sync_event_ptr;
}CallbackPack;
/*
 * Description :
 *  Invokes user supplied callback set as async in Stream::SetSyncCallback().
 *  The AsyncCallbackInvoker is responsible for 'delete'ing arg.
 */
void* AsyncCallbackInvoker(void *arg) {
  CallbackPack *cbp = (CallbackPack *)arg;

  SynchronizationEvent *passed_sync_event = cbp->sync_event_ptr;
  void *callback_user_arg = cbp->callback_user_arg;

  SynchronizationEvent se;
  se.time_stamp = passed_sync_event->time_stamp;
  se.unique_id  = passed_sync_event->unique_id;
  se.sync_detection_event_time_stamp = passed_sync_event->sync_detection_event_time_stamp;
  se.skew = passed_sync_event->skew;
  se.result = passed_sync_event->result;

  cbp->sync_callback(se, callback_user_arg);

  /* Free arg */
  delete passed_sync_event;
  delete cbp;

  return NULL;
}

/*
 * Lipsync default constructor
 */
LipSync::LipSync() {
  is_created                 = false;
  s                          = NULL; // Assoc. Stream obj
  progress                   = -1;
  buffered_sample_timestamp  = "";
  buffered_sample_uniqueid   = "";
  buffered_video_ptr         = NULL;
  buffered_audio_ptr         = NULL;
  buffered_num_frames        = 0;
  buffered_num_audio_samples = 0;
  buffered_seconds           = 0;
#if _WIN32
  process_sample_mutex       = NULL;
#else
  process_sample_mutex       = PTHREAD_MUTEX_INITIALIZER;
#endif /* _WIN32 */
  classifiers_path           = "";
  networks[0]                = NULL;
  networks[1]                = NULL;
  networks[2]                = NULL;
  networks[3]                = NULL;

  callback_threads.clear();

  /* Initialize synchronization varibles */
  /* Lets stick to the way mutexes are init'd and destroy'd by Stream, init in
   * constructor and destroy in destructor.
   */
  bool mutex_created = LSCreateMutex(&process_sample_mutex);
  assert(mutex_created && "Cannot Create Mutex");
}

/*
 * LipSync default destructor.
 */
LipSync::~LipSync() {
  bool mutex_destroyed = LSDestroyMutex(&process_sample_mutex);
  assert(mutex_destroyed && "Cannot destroy mutex");
}

/*
 * Description :
 *   Load all networks
 */
bool LipSync::LoadNetworks() {
#if _WIN32
#define SPLIT_CHAR '\\'
#else
#define SPLIT_CHAR '/'
#endif /* _WIN32 */

  /* Check classifiers exists in given path */
  if (CheckClassifierPathExists(classifiers_path) == 1) {
   DEBUG_INFO("Classifier Path Not exists");
   return false;
  }

  /* Path of classifiers */
  std::string speech_net_str = classifiers_path +
                               SPLIT_CHAR + "ModelA.checkpoint";
  std::string face_net_str = classifiers_path +
                             SPLIT_CHAR + "ModelB.checkpoint";
  std::string lip_net_str  = classifiers_path +
                             SPLIT_CHAR + "ModelC.checkpoint";
  std::string lip_mvt_net_str = classifiers_path +
                                SPLIT_CHAR + "ModelD.checkpoint";

  /* Load Networks */
  /* =========================================================== */

  /* Load Speech Network */
  char speech_net[1024];
  strcpy(speech_net, speech_net_str.c_str());

  /* Check Model exists */
  if (FileExist(speech_net) == 0) {
    DEBUG_INFO("Model Not exists");
    return false;
  }

  int batch = 0;

  /* Stream based approach */
#if GPU_SPEECH_VAL
  batch = 32;
#else
  batch = 64;
#endif

  /* Initialise Speech Network */
  networks[0] = InitializeNetwork(speech_net, batch);
  assert(networks[0] != NULL);

  /* =========================================================== */

  /* Load Face Network */
  char face_net[1024];
  strcpy(face_net, face_net_str.c_str());

  /* Check Model exists */
  if (FileExist(face_net) == 0) {
    DEBUG_INFO("Model Not exists");
    return false;
  }

  /* Initialise Face Network */
  networks[1] = InitializeNetwork(face_net, 1);
  assert(networks[1] != NULL);

  /* =========================================================== */

  /* Load Lip Network */
  char lip_net[1024];
  strcpy(lip_net, lip_net_str.c_str());

  /* Check Model exists */
  if (FileExist(lip_net) == 0) {
    DEBUG_INFO("Model Not exists");
    return false;
  }

  /* Initialise Lip Network */
  networks[2] = InitializeNetwork(lip_net, 1);
  assert(networks[2] != NULL);

  /* =========================================================== */

  /* Load Lip Movement Network */
  char lip_mvt_net[1024];
  strcpy(lip_mvt_net, lip_mvt_net_str.c_str());

  /* Check Model exists */
  if (FileExist(lip_mvt_net) == 0) {
    DEBUG_INFO("Model Not exists");
    return false;
  }

  /* Initialise Lip Movement Network */
  networks[3] = InitializeNetwork(lip_mvt_net, 1);
  assert(networks[3] != NULL);

  /* =========================================================== */

  return true;
}
#undef SPLIT_CHAR

/*
 * Description :
 *   Unload all networks
 */
bool LipSync::UnloadNetworks() {

  /* Release Speech Network */
  FreeNetwork(networks[0]);
  /* Release Face Network */
  FreeNetwork(networks[1]);
  /* Release Lip Network */
  FreeNetwork(networks[2]);
  /* Release Lip Movement Network */
  FreeNetwork(networks[3]);

  /* Assign NULL Pointer to networks */
  networks[0] = NULL;
  networks[1] = NULL;
  networks[2] = NULL;
  networks[3] = NULL;

  return true;
}

/*
 * Description:
 *  Sets LipSync object to created state.
 *
 * Error:
 *  1. LS_LIPSYNCOBJ_ALREADY_CREATED_ERR - Calling LipSync::Create() on a LipSync
 *     object that is already Create()'d.
 *  2. LSERR_CLASSIFIERS_NOT_FOUND - LipSync could not find classifiers
 *
 * Return LSERR_SUCCESS on success.
 */
LSError_t LipSync::Create() {
  if(is_created) {
    /* Already Create()'d */
    return LSERR_LIPSYNCOBJ_ALREADY_CREATED_ERR;
  }

  /* If it is the first Create called on the LipSync object; then all variables
   * would have been initialized by the default const.If this is not the first
   * Create on the LipSync object then all variables would have been properly
   * freed and initialized by LipSync::Destroy
   */
  /* assert for initilized state */
  assert(is_created                 == false);
  assert(s                          == NULL);
  assert(progress                   == -1);
  assert(callback_threads.size()    == 0);
  assert(buffered_sample_timestamp  == "");
  assert(buffered_sample_uniqueid   == "");
  assert(buffered_video_ptr         == NULL);
  assert(buffered_audio_ptr         == NULL);
  assert(buffered_num_frames        == 0);
  assert(buffered_num_audio_samples == 0);
  assert(buffered_seconds           == 0);
  assert(classifiers_path           == "");
  assert(networks[0]                == NULL);
  assert(networks[1]                == NULL);
  assert(networks[2]                == NULL);
  assert(networks[3]                == NULL);

  /* Change only the state of is_created */
  is_created = true;

  /* Get classifier path from  LIPSYNC_CLASSIFIERS_PATH Environment Variable*/
  classifiers_path = GetEnvironmentPath("LIPSYNC_CLASSIFIERS_PATH");
  if (CheckClassifierPathExists(classifiers_path) == 1) {

    /* Check for default classifier path */
    if (CheckClassifierPathExists(DEFAULT_CLASSIFIERS_PATH) == 1) {

      classifiers_path = "";
      is_created = false;
      return LSERR_CLASSIFIERS_NOT_FOUND;

    } else {

      /* Set default classifier path in absences of LIPSYNC_CLASSIFIERS_PATH */
      classifiers_path = DEFAULT_CLASSIFIERS_PATH;
    }
  }

  /* Load Networks */
  bool load_networks = LoadNetworks();
  assert(load_networks == true);

  return LSERR_SUCCESS;
}

/*
 * Description :
 *  Destroys the LipSync object.
 *  The LipSync object may be destroyed only when the object is Create()'d
 *  (is_created is True) and when there is no Stream associated with the LipSync
 *  object.
 *
 * Error :
 *  1. LSERR_LIPSYNCOBJ_NOT_CREATED_ERR - Calling LipSync::Destroy() when the
 *     object is not even Create()'d
 *  2. LSERR_LIPSYNCOBJ_ASSOC_ERR - Calling LipSync::Destroy() when the object
 *     is Create()'d and has a Stream associated with it.
 *
 * Note :
 *  If the LipSync object is in association with a Stream object, It is Required
 *  that LipSync::ClearStream() be called before LipSync::Destroy().
 *
 * Return LSERR_SUCCESS on Success.
 */
LSError_t LipSync::Destroy() {

  if(!is_created) {
    /* Create() not called yet ! */
    return LSERR_LIPSYNCOBJ_NOT_CREATED_ERR;
  }
  if(IsStreamAssociated()) {
    /* Lipsync obj. is aasociated with a stream
     * object; Call ClearStream() before attempting
     * to Destroy the object.
     */
    return LSERR_LIPSYNCOBJ_ASSOC_ERR;
  }

  assert(is_created == true);
  assert(s == NULL);
  assert(progress == -1); /* ProcessSample always makes this -1 on exit */
  /* All Stream related buffered data will be
   * cleared during ClearStream.
   */
  assert(callback_threads.size()    == 0);
  assert(buffered_sample_timestamp  == "");
  assert(buffered_sample_uniqueid   == "");
  assert(buffered_video_ptr         == NULL);
  assert(buffered_audio_ptr         == NULL);
  assert(buffered_num_frames        == 0);
  assert(buffered_num_audio_samples == 0);
  assert(buffered_seconds           == 0);

  /* Unload the Networks */
  bool unload_networks = UnloadNetworks();
  assert(unload_networks == true);

  assert(networks[0]                == NULL);
  assert(networks[1]                == NULL);
  assert(networks[2]                == NULL);
  assert(networks[3]                == NULL);

  /* Change classifiers_path to default state */
  classifiers_path = "";

  /* Mark state as destroyed */
  is_created = false;

  return LSERR_SUCCESS;
}

/*
 * Argument 1 : s - Pointer to Stream object.
 *
 * Descripton :
 *  Creates an association between the Stream object and the LipSync object.
 *  At any point in time only one Stream object can/should be associated with a
 *  LipSync object and vice-versa. A 1-1 mapping between Stream and LipSync objects
 *  is enforced.
 *  Associate a Stream to a LipSync object only when,
 *    1. Create() has been called on this LipSync object.
 *    2. Create() has been called on s (Stream object).
 *    3. s is not associated with any other LipSync object.(To enforce 1-1
 *       mapping)
 *    4. This LipSync object is not associated with any other Stream object. (To
 *       enforce 1-1 mapping)
 *    5. The Stream has sensible and supported AudioProperties
 *    6. The Stream has sensible and supported VideoProperites
 *
 * Error :
 *  1. LSERR_LIPSYNCOBJ_NOT_CREATED_ERR - Calling LipSync::SetStream() before
 *     the LipSync object is even Create()'d.
 *  2. LSERR_INVALID_ARG_ERR - Argument 1, 's', is NULL.
 *  3. LSERR_STREAMOBJ_NOT_CREATED_ERR - Calling LipSync::SetStream() before
 *     the Stream object is Create()'d.
 *  4. LSERR_LIPSYNCOBJ_ASSOC_ERR - Calling LipSync::SetStream() on a LipSync
 *     object that is already associated with another Stream object.
 *  5. LSERR_STREAMOBJ_ASSOC_ERR - Calling LipSync::SetStream() on a LipSync
 *     object with a Stream object that is already associated with someother
 *     LipSync object.
 *  6. LSERR_UNSUPPORTED_AUDIOPROPERTIES_ERR - Calling LipSync::SetStream()
 *     with a Stream the does not have sensible/supported AudioProperties.
 *  7. LSERR_UNSUPPORTED_VIDEOPROPERTIES_ERR - Calling LipSync::SetStream()
 *     with a Stream that does not have sensible/supported VideoProperties.
 *
 * Note :
 *  It is required that the Stream pointer not be freed until LipSync::ClearStream()
 *  is called.
 *
 * Return LSERR_SUCCESS on Success
 */
LSError_t LipSync::SetStream(Stream *s_) {
  if(!is_created) {
    /* LipSync not created ! */
    return LSERR_LIPSYNCOBJ_NOT_CREATED_ERR;
  }
  if(s_ == NULL) {
    /* Invalid argument */
    return LSERR_INVALID_ARG_ERR;
  }
  if(!s_->IsCreated()) {
    /* Cannot set a Stream that is not created */
    return LSERR_STREAMOBJ_NOT_CREATED_ERR;
  }

  /* TODO: Add comments */
  if (s_->IsCancelStateSet()) {
    return LSERR_STREAMOBJ_CANCEL_PROCESS_INITIATED;
  }

  /* LipSync and Stream both are created */
  if(IsStreamAssociated()) {
    /* LipSync obj is already associated */
    return LSERR_LIPSYNCOBJ_ASSOC_ERR;
  }

  if(s_->IsLipSyncAssociated()) {
    /* Stream is already associated */
    return LSERR_STREAMOBJ_ASSOC_ERR;
  }

  if(!IsAudioPropertiesSupported(s_->audio_properties)) {
    /* Stream's AudioProperties is nonsensical/not-supported */
    return LSERR_UNSUPPORTED_AUDIOPROPERTIES_ERR;
  }

  if(!IsVideoPropertiesSupported(s_->video_properties)) {
    /* Stream's VideoProperties is nonsensical/not-supported */
    return LSERR_UNSUPPORTED_VIDEOPROPERTIES_ERR;
  }

  /* Assert that there is no buffered data */
  assert(buffered_sample_timestamp  == "");
  assert(buffered_sample_uniqueid   == "");
  assert(buffered_video_ptr         == NULL);
  assert(buffered_audio_ptr         == NULL);
  assert(buffered_num_frames        == 0);
  assert(buffered_num_audio_samples == 0);
  assert(buffered_seconds           == 0);
  assert(progress                   == -1);

  /* Assert that there are no callback threads */
  assert(callback_threads.size()   == 0);

  /* Assert that Stream is not associated indeed */
  assert(s_->l == NULL);
  assert(s == NULL);

  /* Make association */
  s = s_;
  s_->l = this;

  return LSERR_SUCCESS;
}

/**
 * Description:
 *   Cancel the Stream
 * TODO: Add comments
 */
LSError_t LipSync::CancelStream() {
  if(!is_created) {
    /* LipSync not created ! */
    return LSERR_LIPSYNCOBJ_NOT_CREATED_ERR;
  }

  /* LipSync is created */
  if(!IsStreamAssociated()) {
    /* LipSync obj is not associated */
    return LSERR_LIPSYNCOBJ_NOT_ASSOC_ERR;
  }

  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {
    return LSERR_STREAMOBJ_CANCEL_PROCESS_INITIATED;
  }

  bool mutex_cancel_lock_acquired, mutex_process_lock_acquired;
  bool mutex_cancel_lock_released, mutex_process_lock_released;

  assert(s != NULL);
  /* Lock mutex */
  mutex_cancel_lock_acquired = LSAcquireMutexLock(&s->cancel_mutex);
  assert(mutex_cancel_lock_acquired && "Cannot acqurie mutex lock");

  assert(s != NULL);
  DEBUG_INFO("Setting cancel_in_progress to true\n");
  s->cancel_in_progress = true;

  /* Unlock mutex */
  mutex_cancel_lock_released = LSReleaseMutexLock(&s->cancel_mutex);
  assert(mutex_cancel_lock_released && "Cannot unlock mutex");

  /* Lock mutex */
  mutex_process_lock_acquired = LSAcquireMutexLock(&process_sample_mutex);
  assert(mutex_process_lock_acquired && "Cannot acqurie mutex lock");

  /* If there is any leftover buffered data by the Stream, Release it. */
  ResetBuffer();

  /* Join callback threads if any */
  JoinAllCallbackThreads();
  assert(callback_threads.size() == 0);

  Stream *ss = s;

  assert(s != NULL);

  /* Clear all events */
  bool mutex_sync_events_lock_acquired, mutex_sync_events_lock_released;

  /* Lock mutex */
  mutex_sync_events_lock_acquired = LSAcquireMutexLock(&(s->sync_events_mutex));
  assert(mutex_sync_events_lock_acquired && "Cannot acqurie mutex lock");

  s->sync_events.clear();

#if ENABLE_NETLICENSING
  if (s->IsLicenseeValid()) {

    /* If minute cost is not equal to 0, call NetLicensing validate call */
    if (s->minute_cost != 0) {
      NETLICENSING_LS_INFO("LipSync::CancelStream() function - Minute cost is not zero\n");
      LSError_t result_status = NetLicensingLSValidate(s->licensee_number,
                                                       ceil(s->minute_cost));

      /* If result is not valid, Change is_licensee_valid flag */
      if (result_status != LSERR_SUCCESS) {

        NETLICENSING_LS_INFO("LipSync::CancelStream() function - Set is_licensee_valid flag as false\n");

        bool mutex_licensee_lock_acquired, mutex_licensee_lock_released;
        /* Lock the mutex */
        mutex_licensee_lock_acquired = LSAcquireMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_acquired && "Cannot lock mutex");

        s->is_licensee_valid   = false;

        /* Unlock the mutex */
        mutex_licensee_lock_released = LSReleaseMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_released && "Cannot release mutex");

      }
    } // minute_cost if

  } else {
    NETLICENSING_LS_INFO("LipSync::CancelStream() function - Licensee is not valid\n");
  }
#endif

  /* Assign minute_cost as 0 */
  s->minute_cost = 0.0;

  /* Unlock mutex */
  mutex_sync_events_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
  assert(mutex_sync_events_lock_released && "Cannot unlock mutex");

  assert(s != NULL);
  assert(s->l != NULL);

  /* Clear the Stream */
  s->l = NULL;
  s = NULL;

  /* Lock mutex */
  mutex_cancel_lock_acquired = LSAcquireMutexLock(&ss->cancel_mutex);
  assert(mutex_cancel_lock_acquired && "Cannot acqurie mutex lock");

  assert(ss != NULL);
  DEBUG_INFO("Setting cancel_in_progress to false\n");
  ss->cancel_in_progress = false;

  /* Unlock mutex */
  mutex_cancel_lock_released = LSReleaseMutexLock(&ss->cancel_mutex);
  assert(mutex_cancel_lock_released && "Cannot unlock mutex");

  /* Unlock mutex */
  mutex_process_lock_released = LSReleaseMutexLock(&process_sample_mutex);
  assert(mutex_process_lock_released && "Cannot unlock mutex");

  return LSERR_SUCCESS;
}

/*
 * Description :
 *  Dissociate the Stream already associated. Any asynchronous callback threads
 *  that have been spawned by this LipSync object is joined;
 *  LipSync::ClearStream() has to,
 *   1. Dissociate the stream already associated.
 *   2. Release any left-out buffer contents.
 *   3. Join all callbacks.
 *
 * Error:
 *  1. LSERR_LIPSYNCOBJ_NOT_CREATED_ERR - Calling LipSync::ClearStream() when
 *     LipSync is not even Create()'d.
 *  2. LSERR_LIPSYNCOBJ_NOT_ASSOC_ERR - Calling LipSync::ClearStream() when
 *     there is no Stream to clear.
 *
 * Return LSERR_SUCCESS on Success.
 */
LSError_t LipSync::ClearStream() {

  bool mutex_lock_acquired;
  bool mutex_lock_released;
  /* Lock mutex; Do nothing when the ProcessSample in execution */
  mutex_lock_acquired = LSAcquireMutexLock(&process_sample_mutex);
  assert(mutex_lock_acquired && "Cannot acquire mutex lock");

  if(!is_created) {
    /* LipSync not created */

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_LIPSYNCOBJ_NOT_CREATED_ERR;
  }
  if(!IsStreamAssociated()) {
    /* No Stream to clear */

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_LIPSYNCOBJ_NOT_ASSOC_ERR;
  }
  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_STREAMOBJ_CANCEL_PROCESS_INITIATED;
  }


  /* If there is any leftover buffered data by the Stream, Release it. */
  ResetBuffer();

  /* Join callback threads if any */
  JoinAllCallbackThreads();
  assert(callback_threads.size() == 0);

  /* Set 0 to minute cost */
  bool mutex_sync_events_lock_acquired, mutex_sync_events_lock_released;

  /* Lock the mutex */
  mutex_sync_events_lock_acquired = LSAcquireMutexLock(&(s->sync_events_mutex));
  assert(mutex_sync_events_lock_acquired && "Cannot acqurie mutex lock");

#if ENABLE_NETLICENSING
  if (s->IsLicenseeValid()) {

    /* If minute cost is not equal to 0, call NetLicensing validate call */
    if (s->minute_cost != 0) {

      NETLICENSING_LS_INFO("LipSync::ClearStream() function - minute_cost is not zero\n");

      LSError_t result_status = NetLicensingLSValidate(s->licensee_number,
                                                       ceil(s->minute_cost));

      /* If result is not valid, Change is_licensee_valid flag */
      if (result_status != LSERR_SUCCESS) {

        bool mutex_licensee_lock_acquired, mutex_licensee_lock_released;
        /* Lock the mutex */
        mutex_licensee_lock_acquired = LSAcquireMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_acquired && "Cannot lock mutex");

        s->is_licensee_valid   = false;

        /* Unlock the mutex */
        mutex_licensee_lock_released = LSReleaseMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_released && "Cannot release mutex");

      }
    } // minute_cost if

  } else {
    NETLICENSING_LS_INFO("LipSync::ClearStream() function - Licensee is not valid\n");
  }
#endif

  /* Assign minute_cost as 0 */
  s->minute_cost = 0.0;

  /* Unlock the mutex */
  mutex_sync_events_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
  assert(mutex_sync_events_lock_released && "Cannot unlock mutex");

  assert(s != NULL);
  assert(s->l != NULL);

  /* Clear the Stream */
  s->l = NULL;
  s = NULL;

  /* Unlock mutex */
  mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
  assert(mutex_lock_released && "Cannot release mutex lock");

  return LSERR_SUCCESS;
}


/*
 * Description :
 *  Returns a value between [0-100] denoting the progress of LipSync::ProcessSample()
 *  LipSync::ProcessSample() is the only entity that updates LipSync::Progress.
 *  When ProcessSample starts it sets LipSync::Progress to 0 and when it completes
 *  it sets it back to -1.
 *
 * Returns [0-100] indicating progress as a percentage
 * Returns -1 if LipSync is not processing any sample.
 */
int LipSync::GetProgress() {
  if(!is_created) {
    /* LipSync not created; Progress makes no sense; */
    return -1;
  }
  if(!IsStreamAssociated()) {
    /* No Stream associated; Progress makes no sense */
    return -1;
  }
  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {
    return -2;
  }
  return progress;
}
#if REMOVE_DISK_ACCESS_FACE
/**
 * Description:
 *   Compare Two Mat images (Pixel wise)
 */
bool images_comparsion(cv::Mat image_1, cv::Mat image_2, int channels) {

  if (!image_1.data || !image_2.data) {
    return false;
  }

  /* Compare width and height of Mat */
  if (image_1.rows != image_2.rows || image_1.cols != image_2.cols) {
    std::cout << "Dimensions mismatch" << std::endl;
    return false;
  }

  /* Compare Pixels */
  for (int col = 0; col < image_1.cols; col++) {
    for (int row = 0; row < image_1.rows; row++) {
      /* Get RGB Values */
      Vec3b rgb_1 = image_1.at<Vec3b>(row, col);
      Vec3b rgb_2 = image_2.at<Vec3b>(row, col);

      for (int c = 0; c < image_1.channels(); c++) {

        if (rgb_1.val[c] != rgb_2.val[c]) {
          std::cout << "Pixels mismatch" <<std::endl;
          return false;
        }

      }
    }
  }

  return true;
}

/**
 * Description:
 *    test_GetTimestampFaces() checks with process_face_main() results.
 *    Checks:
 *      1) Compare Timestamp count
 *      2) Compare Timestamp name
 *      3) Compare Face folder count
 *      4) Compare Face images
 *      5) Compare Probability values
 */
bool test_GetTimestampFaces(std::string cache_dir,
                            std::vector<TimestampFace> timestamp_face_list,
                            Buffer video,
                            VideoProperites video_properties) {

#if _WIN32
  #define SPLIT_CHAR '\\'
#else
  #define SPLIT_CHAR '/'
#endif /* _WIN32 */

  /* Compare Timestamp list */
  std::vector<std::string> cache_folder_timestamp_path = PatternSearch(cache_dir +
                                                                       "?-??-??.???" +
                                                                       SPLIT_CHAR);

  /* If Cache folder contains timestamps, store empty vector */
  if (cache_folder_timestamp_path[0] == "" &&
      cache_folder_timestamp_path.size() == 1) {
    cache_folder_timestamp_path = std::vector<std::string>();
  }

  /* Get timestamp count */
  int timestamp_folder_count = cache_folder_timestamp_path.size();

  /* Compare timestamp count */
  if (timestamp_folder_count != timestamp_face_list.size()) {
    std::cout << "Timestamp count differs" << std::endl;
    return false;
  }

  /* Get and set VideoProp */
  VideoProp *video_prop;
  CHECKED_MALLOC(video_prop, VideoProp, 1);

  /* Get fps using video time scale and video sample duration */
  double fps = ((double)video_properties.time_resolution.video_time_scale) /
                       ((double)video_properties.time_resolution.video_sample_duration);
  /* Get frame height and width using video_properties */
  int height = video_properties.frame_height;
  int width = video_properties.frame_width;

  /* RGB colorspace */
  int channels = 3;
  unsigned long long frame_size = height * width * channels;
  int frame_count = video.buffer_size / frame_size;
  SetVideoProperties(video_prop, height, width, channels, fps, frame_count);

  /* Get TimeStamp from path */
  std::vector<std::string> timestamps;
  for (unsigned int iter = 0; iter < timestamp_folder_count; iter++) {

    int success = 0;
    std::string timestamp = SplitString(cache_folder_timestamp_path[iter],
                                        SPLIT_CHAR,
                                        -1,
                                        &success);

    /* Compare face list timestamp with cache folder timestamp */
    if (timestamp != timestamp_face_list[iter].timestamp) {
      std::cout << "Timestamp differs" << std::endl;
      std::cout << "process_face_main() timestamp count = "
                <<  timestamp_face_list[iter].timestamp << std::endl;
      std::cout << "GetTimestampFaces() timestamp count = "
                << timestamp << std::endl;
      free(video_prop);
      return false;
    }

    /* Get Face folder list */
    std::vector<std::string> face_folders =
                            PatternSearch(cache_folder_timestamp_path[iter] +
                                          "face_*" +
                                          SPLIT_CHAR);

    /* If timestamp contains face folders, store empty vector */
    if (face_folders[0] == "" && face_folders.size() == 1) {
      face_folders = std::vector<std::string>();
    }

    /* Compare Face count */
    if (face_folders.size() != timestamp_face_list[iter].faces.size()) {
      std::cout << "Face Folder differs" << std::endl;
      std::cout << "Timestamp:" << timestamp << std::endl;
      std::cout << "process_face_main() face count = "
                << face_folders.size() << std::endl;
      std::cout << "GetTimestampFaces() face count = "
                << timestamp_face_list[iter].faces.size() << std::endl;
      std::cout << "Face folder:" << face_folders[0] << std::endl;
      free(video_prop);
      return false;
    }

    /* Converting the timestamp string to time in seconds */
    long double time = GetTimeFromString((char*)timestamp.c_str());

    /*  Get Sample point in video buffer */
    VideoSample *video_sample;
    video_sample = GetVideoSample(video_prop, time, video.buffer_data);

    /* Get face folder count */
    unsigned int face_count = face_folders.size();

    /* Read face_probs.txt */
    std::string face_cache_probs_txt = cache_folder_timestamp_path[iter] +
                                       "face_probs.txt";
    std::vector<std::string> read_probs = ReadFile(face_cache_probs_txt);
    std::string face_probs_line;

    /* check for empty file */
    if (read_probs.size() == 0) {
      face_probs_line = "";
    } else {
      face_probs_line = read_probs[0];
    }

    /* Convert string to float probability */
    std::vector<std::string> face_probs_str = GenerateListOfStrings(face_probs_line, ',');
    std::vector<float> face_probs;
    for (unsigned int prob_iter = 0; prob_iter < face_probs_str.size(); prob_iter++) {
      face_probs.push_back(atof(face_probs_str[prob_iter].c_str()));
    }


    /* Compare Face images */
    for (unsigned int face_iter = 0; face_iter < face_count; face_iter++) {
      /* Get Image names */
      int success = 0;

      /* store face folder */
      std::string face_folder = face_folders[face_iter];
      Face face = timestamp_face_list[iter].faces[face_iter];

      std::vector<std::string> images_path = PatternSearch(face_folder + "*.png");

      /* If no images present in face folder, store empty vector */
      if (images_path[0] == "" && images_path.size() == 1) {
        images_path = std::vector<std::string>();
      }

      /* Get Image Count */
      unsigned int images_count = images_path.size();

      /* Get Start and end face id */
      std::string images_start_image = SplitString(images_path[0],
                                                   SPLIT_CHAR,
                                                   -1,
                                                   &success);
      std::string images_end_image = SplitString(images_path[images_count - 1],
                                                 SPLIT_CHAR,
                                                 -1,
                                                 &success);
      int start_image_id = atoi(SplitString(images_start_image,
                                            '.',
                                            0,
                                            &success).c_str());

      int end_image_id = atoi(SplitString(images_end_image,
                                          '.',
                                          0,
                                          &success).c_str());

      /* Get Start and End Box id  */
      int fps_int = int(ceil(fps));

      int start_box_id = FetchFirstValidBoxPosition(face.boxes);
      int end_box_id   = FetchLastValidBoxPosition(face.boxes);

      /*
      std::cout << "Start Image id :" << start_image_id << std::endl;
      std::cout << "End Image id   :" << end_image_id << std::endl;
      std::cout << "Start Box id   :" << start_box_id << std::endl;
      std::cout << "End Box id     :" << end_box_id << std::endl;
      std::cout << std::endl;
      */

      if (start_image_id != start_box_id || end_image_id != end_box_id) {
        std::cout << "Face image count differs" << std::endl;
        std::cout << "Timestamp   = " << timestamp << std::endl;
        std::cout << "Face folder = " << face_folder << std::endl;
        free(video_prop);

        /* Free video sample */
        FreeVideoSample(video_sample);

        return false;
      }

      /* Get 2 second frames from video buffer */
      std::vector<cv::Mat> frames(2*fps_int);
      for (int frame_iter = 0;
           frame_iter < (2*fps_int);
           frame_iter ++) {
        Mat temp_image(height, width, CV_8UC3,
                       video_sample->data + (frame_size * frame_iter));
        cvtColor(temp_image, frames[frame_iter], CV_RGB2BGR);
      }

      /* Compare face images between methods */
      unsigned int image_iter = 0;
      for (unsigned int box_id = start_box_id;
           box_id <= end_box_id && image_iter <= images_count;
           box_id ++) {
        /* Load face image */
        cv::Mat face_cache_image = cv::imread(images_path[image_iter],
                                              CV_LOAD_IMAGE_COLOR);

        /* Get face crop */
        int crop_ret_status = -99;
        cv::Mat face_crop = CropImage(frames[box_id],
                                      face.boxes[box_id],
                                      crop_ret_status);

        if (crop_ret_status == 1) {
          std::cout << "Crop Image fails" << std::endl;
          free(video_prop);
          /* Free video sample */
          FreeVideoSample(video_sample);

          return false;
        }

        /* Compare cropped image and original image */
        if (!images_comparsion(face_crop, face_cache_image, channels)) {
          std::cout << images_path[image_iter] << " differs" << std::endl;
          free(video_prop);
          /* Free video sample */
          FreeVideoSample(video_sample);

          return false;
        }
        image_iter ++;
      }

      /* Compare Probability */
      if (face.probability != face_probs[face_iter]) {
        std::cout << "Probability mismatch" << std::endl;
        std::cout << "Face Folder = " << cache_folder_timestamp_path[iter] << std::endl;
        std::cout << "process_face_main() probs = " << face_probs[face_iter] << std::endl;
        std::cout << "GetTimestampFaces() probs = " << face.probability << std::endl;
        free(video_prop);
        /* Free video sample */
        FreeVideoSample(video_sample);
        return false;
      }

    }

    /* Free video sample */
    FreeVideoSample(video_sample);
  }

  free(video_prop);

  /* Success termination */
  return true;
}
#undef SPLIT_CHAR
#endif // REMOVE_DISK_ACCESS_FACE

#if REMOVE_DISK_ACCESS_TRACKFACE
/**
 * Description:
 *    test_TrackFace() checks with process_trackface() results.
 *    Checks:
 *      1) Compare Timestamp count
 *      2) Compare Timestamp name
 *      3) Compare Face folder count
 *      4) Compare Face images
 */
bool test_TrackFace(std::string cache_dir,
                    std::vector<TimestampFace> timestamp_face_list,
                    Buffer video,
                    VideoProperites video_properties) {

#if _WIN32
  #define SPLIT_CHAR '\\'
#else
  #define SPLIT_CHAR '/'
#endif /* _WIN32 */

  /* Compare Timestamp list */
  std::vector<std::string> cache_folder_timestamp_path = PatternSearch(cache_dir +
                                                                       "?-??-??.???" +
                                                                       SPLIT_CHAR);

  /* If Cache folder contains timestamps, store empty vector */
  if (cache_folder_timestamp_path[0] == "" &&
      cache_folder_timestamp_path.size() == 1) {
    cache_folder_timestamp_path = std::vector<std::string>();
  }

  /* Get timestamp count */
  unsigned int timestamp_folder_count = cache_folder_timestamp_path.size();

  /* Compare timestamp count */
  if (timestamp_folder_count != timestamp_face_list.size()) {
    std::cout << "Timestamp count differs" << std::endl;
    return false;
  }

  /* Get and set VideoProp */
  VideoProp *video_prop;
  CHECKED_MALLOC(video_prop, VideoProp, 1);

  /* Get fps using video time scale and video sample duration */
  double fps = ((double)video_properties.time_resolution.video_time_scale) /
                       ((double)video_properties.time_resolution.video_sample_duration);

  /* Get Start and End Box id  */
  int fps_int = int(ceil(fps));

  /* Get frame height and width using video_properties */
  int height = video_properties.frame_height;
  int width = video_properties.frame_width;

  int channels = 3;
  unsigned long long frame_size = height * width * channels;
  int frame_count = video.buffer_size / frame_size;
  SetVideoProperties(video_prop, height, width, channels, fps, frame_count);

  /* Get TimeStamp from path */
  std::vector<std::string> timestamps;
  for (unsigned int iter = 0; iter < timestamp_folder_count; iter++) {

    int success = 0;
    std::string timestamp = SplitString(cache_folder_timestamp_path[iter],
                                        SPLIT_CHAR,
                                        -1,
                                        &success);
#if LS_DEBUG
    std::cout << "Timestamp = " << timestamp << std::endl;
#endif // LS_DEBUG

    /* Compare face list timestamp with cache folder timestamp */
    if (timestamp != timestamp_face_list[iter].timestamp) {
      std::cout << "Timestamp differs" << std::endl;
      std::cout << "process_trackface() timestamp count = "
                <<  timestamp_face_list[iter].timestamp << std::endl;
      std::cout << "Tracface()          timestamp count = "
                << timestamp << std::endl;
      free(video_prop);
      return false;
    }

    /* Get Face folder list */
    std::vector<std::string> face_folders =
                            PatternSearch(cache_folder_timestamp_path[iter] +
                                          "tface_*" +
                                          SPLIT_CHAR);

    /* If timestamp contains face folders, store empty vector */
    if (face_folders[0] == "" && face_folders.size() == 1) {
      face_folders = std::vector<std::string>();
    }

    /* Get face count */
    unsigned int face_count = face_folders.size();

    /* Get track face id from cache folder */

    std::vector<int> track_face_id;
    /* Iterate face in timestamp */
    for (unsigned int face_iter = 0;
         face_iter < face_count;
         face_iter ++) {
      std::string face_folder = SplitString(face_folders[face_iter],
                                            SPLIT_CHAR,
                                            -1,
                                            &success);
#if LS_DEBUG
      std::cout << "TFace folder = "  << face_folder << std::endl;
#endif // LS_DEBUG

      int face_id = atoi(SplitString(face_folder,
                                     '_',
                                     -1,
                                     &success).c_str());
#if LS_DEBUG
      std::cout << "Face id = " << face_id << std::endl;
#endif // LS_DEBUG

      track_face_id.push_back(face_id);

    } // Iterate face

    unsigned int trackface_count = timestamp_face_list[iter].faces.size();

    /* Iterate face */
    for (unsigned int face_iter = 0;
         face_iter < trackface_count;
         face_iter ++) {
      /* Check face id present in track face id */
      if (std::find(track_face_id.begin(), track_face_id.end(), face_iter) !=
                                                      track_face_id.end()) {
        /* Track face id matches */
        if (IsFaceEmpty(timestamp_face_list[iter].faces[face_iter]) == true) {
          std::cout << "Timestamp = " << timestamp << std::endl;
          std::cout << "Tface id mismatches(FP)" << std::endl;
          std::cout << "Tface id  = " << face_iter << std::endl;
          free(video_prop);
          return false;
        }
      } else {
        /* Track face id not matches */
        if (IsFaceEmpty(timestamp_face_list[iter].faces[face_iter]) == false) {
          std::cout << "Timestamp = " << timestamp << std::endl;
          std::cout << "Tface id mismatches(FN)" << std::endl;
          std::cout << "Tface id  = " << face_iter << std::endl;
          free(video_prop);
          return false;
        }

      } // else

    } // Iterate face

    /* Converting the timestamp string to time in seconds */
    long double time = GetTimeFromString((char*)timestamp.c_str());

    /*  Get Sample point in video buffer */
    VideoSample *video_sample;
    video_sample = GetVideoSample(video_prop, time, video.buffer_data);

    /* Compare images */
    for (unsigned int face_iter = 0;
         face_iter < face_count;
         face_iter ++) {
      /* Get Image names */
      int success = 0;

      /* Store face folder */
      std::string face_folder = face_folders[face_iter];

      /* Get Face folder name */
      std::string face_folder_name = SplitString(face_folder,
                                                 SPLIT_CHAR,
                                                 -1,
                                                 &success);
      /* Get face id from tface_* */
      int face_id = atoi(SplitString(face_folder_name,
                                     '_',
                                     -1,
                                     &success).c_str());

      Face face = timestamp_face_list[iter].faces[face_id];

      std::vector<std::string> images_path = PatternSearch(face_folder + "*.png");

      /* If no images present in face folder, store empty vector */
      if (images_path[0] == "" && images_path.size() == 1) {
        images_path = std::vector<std::string>();
      }

      /* Get Image Count */
      int images_count = images_path.size();

      /* Get Start and end face id */
      std::string images_start_image = SplitString(images_path[0],
                                                   SPLIT_CHAR,
                                                   -1,
                                                   &success);
      std::string images_end_image = SplitString(images_path[images_count - 1],
                                                 SPLIT_CHAR,
                                                 -1,
                                                 &success);
      int start_image_id = atoi(SplitString(images_start_image,
                                            '.',
                                            0,
                                            &success).c_str());

      int end_image_id = atoi(SplitString(images_end_image,
                                          '.',
                                          0,
                                          &success).c_str());

#if LS_DEBUG
      std::cout << face_folder << std::endl;
      std::cout << "Start Image id = " << start_image_id << std::endl;
      std::cout << "End Image id   = " << end_image_id << std::endl;
#endif // LS_DEBUG

      int start_box_id = FetchFirstValidBoxPosition(face.boxes);
      int end_box_id   = FetchLastValidBoxPosition(face.boxes);

#if LS_DEBUG
      std::cout << "Start Box id = " << start_box_id << std::endl;
      std::cout << "End Box id   = " << end_box_id << std::endl;
#endif // LS_DEBUG

      /* Compare image first id and last id with boxes */
      if (start_image_id != start_box_id || end_image_id != end_box_id) {
        std::cout << "Face image count differs" << std::endl;
        std::cout << "Timestamp   = " << timestamp << std::endl;
        std::cout << "Face folder = " << face_folder << std::endl;
        free(video_prop);

        /* Free video sample */
        FreeVideoSample(video_sample);

        return false;
      }

      /* Get 2 second frames from video buffer */
      std::vector<cv::Mat> frames(2*fps_int);
      for (int frame_iter = 0;
           frame_iter < (2*fps_int);
           frame_iter ++) {
        Mat temp_image(height, width, CV_8UC3,
                       video_sample->data + (frame_size * frame_iter));
        cvtColor(temp_image, frames[frame_iter], CV_RGB2BGR);
      }

      unsigned int image_iter = 0;
      for (unsigned int box_id = start_box_id;
           box_id <= end_box_id && image_iter <= images_count;
           box_id ++) {

        /* Load face image */
        cv::Mat face_cache_image = cv::imread(images_path[image_iter],
                                              CV_LOAD_IMAGE_COLOR);

        /* Get face crop */
        int crop_ret_status = -99;
        cv::Mat face_crop = CropImage(frames[box_id],
                                      face.boxes[box_id],
                                      crop_ret_status);

        if (crop_ret_status == 1) {
          std::cout << "Crop Image fails" << std::endl;
          free(video_prop);
          /* Free video sample */
          FreeVideoSample(video_sample);

          return false;
        }

        /* Compare cropped image and original image */
        if (!images_comparsion(face_crop, face_cache_image, channels)) {
          std::cout << images_path[image_iter] << " differs" << std::endl;
          free(video_prop);
          /* Free video sample */
          FreeVideoSample(video_sample);

          return false;
        }

        image_iter ++;
      } // Iterate images

    } // Iterate face

    /* Free Videosample */
    FreeVideoSample(video_sample);

  } // Iterate timestamp

  free(video_prop);

  /* Success termination */
  return true;
}
#undef SPLIT_CHAR
#endif // REMOVE_DISK_ACCESS_TRACKFACE

/*
 * Description:
 *  Invoke callbacks on every input SynchronizationEvent
 */
void LipSync::DoCallbacks(std::vector<SynchronizationEvent> se) {

  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {
    return;
  }

  bool mutex_lock_acquired;
  bool mutex_lock_released;
  LSError_t result_status = LSERR_UNSET;

  /* Lock mutex */
  mutex_lock_acquired = LSAcquireMutexLock(&(s->sync_events_mutex));
  assert(mutex_lock_acquired && "Cannot acquire mutex lock");

  /*
   * If minute is greater than 1, Call validate function to reduce
   * the minutes.
   */

#if ENABLE_NETLICENSING
  if (s->IsLicenseeValid()) {

    if (s->minute_cost >= MINUTE_COST) {

      NETLICENSING_LS_INFO("LipSync::DoCallbacks() function - minute_cost is not zero\n");

      result_status = NetLicensingLSValidate(s->licensee_number,
                                             RoundOffMinutes(s->minute_cost));
      /* Assign minute_cost as 0 */
      s->minute_cost = 0;

      if (result_status != LSERR_SUCCESS) {

        NETLICENSING_LS_INFO("LipSync::DoCallbacks() function - Set is_licensee_valid flag as false\n");

        bool mutex_licensee_lock_acquired, mutex_licensee_lock_released;
        /* Lock the mutex */
        mutex_licensee_lock_acquired = LSAcquireMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_acquired && "Cannot lock mutex");

        s->is_licensee_valid   = false;

        /* Unlock the mutex */
        mutex_licensee_lock_released = LSReleaseMutexLock(&s->licensee_mutex);
        assert(mutex_licensee_lock_released && "Cannot release mutex");

        /* Unlock mutex */
        mutex_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
        assert(mutex_lock_released && "Cannot release mutex lock");
        return;
      }
    }
  } else {

    NETLICENSING_LS_INFO("LipSync::DoCallbacks() function - Licensee is not valid\n");

    /* Assign minute_cost as 0 */
    s->minute_cost = 0;

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
    assert(mutex_lock_released && "Cannot release mutex lock");
    return;
  }
#else
   /* Assign minute_cost as 0 */
   s->minute_cost = 0;
#endif

  /* Unlock mutex */
  mutex_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
  assert(mutex_lock_released && "Cannot release mutex lock");

  /* Invoke call back on every sync_event */
  if(s->sync_callback != NULL) {
    /* User callback is set */

    std::vector<SynchronizationEvent>::iterator it;

    for(it = se.begin(); it != se.end(); it++) {

      /* Check if #callback threads is maxed out */
      if(callback_threads.size() == MAX_CALLBACK_THREADS) {
        /* TODO : Should be optimized */
        JoinAllCallbackThreads();
        assert(callback_threads.size() == 0);
      }

      /* Invoke callback on every SynchronizationEvent detected */
      if(s->is_callback_asynchronous) {

        /* Create synchronization event in heap to pass to callback;
        * Explicitly use new and delete
        */
        SynchronizationEvent *to_pass_sync_event = NULL;
        to_pass_sync_event = new SynchronizationEvent;

        /* Fill to_pass_sync_event with current sync event */
        to_pass_sync_event->time_stamp = it->time_stamp;
        to_pass_sync_event->unique_id  = it->unique_id;
        to_pass_sync_event->sync_detection_event_time_stamp = it->sync_detection_event_time_stamp;
        to_pass_sync_event->skew = it->skew;
        to_pass_sync_event->result = it->result;

        /* Create CallbackPack */
        CallbackPack *cbp = new CallbackPack;
        cbp->sync_callback = s->sync_callback;
        cbp->callback_user_arg = s->callback_user_arg;
        cbp->sync_event_ptr = to_pass_sync_event;

#if _WIN32
        HANDLE thread_stat;
        bool thread_launched = LSCreateThread(&thread_stat, (LPTHREAD_START_ROUTINE)AsyncCallbackInvoker, (void*)cbp);
        assert(thread_launched && "Cannot create thread");
#else
        pthread_t thread_stat;
        bool thread_launched = LSCreateThread(&thread_stat, &AsyncCallbackInvoker, (void*)cbp);
        assert(thread_launched && "Cannot create thread");
#endif

        /* Save thread_stat */
        callback_threads.push_back(thread_stat);

      } else {
        /* Callback is synchronous; */
        s->sync_callback(*it, s->callback_user_arg);
      }

    } // se iterator
  } // is callback set

}

/*
 * Description:
 *  Add input SynchronizationEvents to Stream's SynchronizationEvent list
 */
void LipSync::UpdateStreamSynchronizationEvents(
                                        std::vector<SynchronizationEvent> se) {

  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {
    return;
  }

  bool mutex_lock_acquired;
  bool mutex_lock_released;
  bool is_minute_cost_updated;

  /* Lock mutex */
  mutex_lock_acquired = LSAcquireMutexLock(&(s->sync_events_mutex));
  assert(mutex_lock_acquired && "Cannot acquire mutex lock");

  /* Update s->sync_events */
  for (std::vector<SynchronizationEvent>::iterator it = se.begin();
       it != se.end();
       it++) {

    bool already_present = false;
    std::vector<SynchronizationEvent>::iterator se_iter;
    for (se_iter = s->sync_events.begin();
         se_iter != s->sync_events.end();
         se_iter++) {

      if (it->unique_id == se_iter->unique_id &&
          it->sync_detection_event_time_stamp == se_iter->sync_detection_event_time_stamp) {

        already_present = true;
        break;
      }
    }

    if (!already_present) {
      s->sync_events.push_back(*it);
    }
  }

  /* Add Processed seconds with the Stream's minute_cost */
  is_minute_cost_updated = s->UpdateMinuteCost(buffered_seconds);
  assert(is_minute_cost_updated && "Cannot update minute_cost for the associated stream");

  /* Unlock mutex */
  mutex_lock_released = LSReleaseMutexLock(&(s->sync_events_mutex));
  assert(mutex_lock_released && "Cannot release mutex lock");
}
/************************************************************************************************/
std::string convert_time(int milli){
    int sec = milli/1000;
    int hr = sec / 3600;
    int min = (sec -(3600*hr))/60;
    int m = milli%1000;
    std::string s_hr,s_min,s_sec,s_ms;
    s_hr=to_string(hr);
    if(min<10)
        s_min="0"+to_string(min);
    else
        s_min=to_string(min);
    if(sec<10)
        s_sec="0"+to_string(sec);
    else
        s_sec=to_string(sec);
    if(m<10)
        s_ms="00"+to_string(m);
    else if (m<100)
        s_ms="0"+to_string(m);
    else
        s_ms=to_string(m);
    std::string time = s_hr+"-"+ s_min +"-"+ s_sec +"."+ s_ms;
    return time;
}
/***********************************************************************************************/
void funcVad(Buffer audio,std::vector<TimestampSpeech> *speeches){
    /*
    GetSampledSpeechBits() will take input audio buffer data, run it 
    through CNN and return TimestampSpeech object which will contain 
    timestamp string and audio bits of 0s and 1s. Instead of that 
    funcVad will take the same input and use vad to get the audio bits
    and also the timestamp string, returns TimestampSpeech object
    */
    if (audio.buffer_data == NULL) {
        ErrorLog("\n Audio Pointer is NULL");
        exit(0);        //probably a bad way of exiting!! need improvement
    }
    short* audio_buffer;
    audio_buffer = (short*) audio.buffer_data;
    Fvad *vad = NULL;
    int fvad_samplerate = 48000;         //fvad supports only 48000 currently
    int agressiveness = 3;              //fvad agressiveness can be 1(less),2,3(more) to get audio activity detections
                                        //higher the agressiveness, more finer the sound information will be captured
    vad = fvad_new();                   //initiating vad 
    fvad_set_mode(vad, agressiveness);  //setting the properties aggressiveness 
    fvad_set_sample_rate(vad, fvad_samplerate);
    int total_len = audio.buffer_size/2;         //audio.buffer_size is size of buffer in bytes since we converting it into short datatype /2
    int frame_len= (int)((float)(fvad_samplerate/1000.0) * 30.0);              //sample_rate/1000 * frame_ms
    int total_buffer_size = total_len/frame_len;
    bool vad_output_buffer[total_buffer_size];       //buffer to store output of vad
    int idx=0;
    int channels = 1;
    int lipsync_sample_rate = 44100;       //lipsync sample rate
    int vad_sample_rate = 48000;      //vad sample rate
    int output_size = frame_len * vad_sample_rate/lipsync_sample_rate;
    double step_dist;
    const uint64_t fixed_fraction = (1LL << 32);
    const double norm_fixed = (1.0 / (1LL << 32));
    uint64_t step;
    uint64_t current_offset;
    while(total_len>=frame_len){
        int16_t *buf1 = NULL,*opt = NULL;
        buf1 =(int16_t *) malloc(frame_len * sizeof *buf1);
        opt = (int16_t *) malloc(output_size * sizeof *opt);
        step_dist = ((double) lipsync_sample_rate / (double) vad_sample_rate);
        step = ((uint64_t) (step_dist * fixed_fraction + 0.5));
        current_offset = 0;
        for (int i = 0; i < frame_len; i++)
            buf1[i] = audio_buffer[(idx*frame_len)+i];             //copying audio buffer data segment to temp variable
        /*preprocessing the audio buffer data to convert 44100 to 48000 samplerate data because lipsync supports 44100 sample rate*/
        for (uint32_t i = 0; i < output_size; i += 1) {
            for (uint32_t c = 0; c < channels; c += 1) {
                opt[i] = (int16_t) (buf1[c] + (buf1[c + channels] - buf1[c]) * (
                    (double) (current_offset >> 32) + ((current_offset & (fixed_fraction - 1)) * norm_fixed)));
            }
            current_offset += step;
            buf1 += (current_offset >> 32) * 1;
            current_offset &= (fixed_fraction - 1);
        }
        //vad execution
        vad_output_buffer[idx++]=fvad_process(vad, opt, output_size);                //appending the result to output buffer
        total_len -= frame_len;
        free(opt);
    }

    int count=0,flag=0;
    // smoothening of the curve if there is a small fluctuation in 0s and 1s smoothen it by replacing the last stable value
    for(int i=1;i<total_buffer_size;i++){
        flag=0;
        if(vad_output_buffer[i-1]!=vad_output_buffer[i]){
            count=0;
            for(int j=i+1;j<total_buffer_size;j++){
                if(count==3)            //emperically set to 3
                    break;
                if(vad_output_buffer[j]==vad_output_buffer[i])
                    count++;
                else{
                    if(count!=4){
                        flag=1;
                        break;
                    }
                }
            }
        }
        if(flag==1){
            for(int j=0;j<count;j++){
                vad_output_buffer[i+j]=vad_output_buffer[i-1];
                i = i +count - 1;
            }
        }
    }
    TimestampSpeech e;
    //prepare output timestamps from output buffer
    for(int i=1;i<total_buffer_size;i++){
        if(vad_output_buffer[i-1]!=vad_output_buffer[i] && i>24 && i< total_buffer_size-24){     //check for slope i.e., 0 to 1 or 1 to 0
            e.timestamp=convert_time(round(i*30));          //get timestamp in format hr-min-sec.ms given milliseconds
            e.speech_bits="";
            for(int j=i-24;j<i+24;j++){
                if(j<0)
                    e.speech_bits.append(std::to_string(vad_output_buffer[0]));
                else if(j>total_buffer_size)
                    e.speech_bits.append(std::to_string(vad_output_buffer[total_buffer_size-1]));
                else
                    e.speech_bits.append(std::to_string(vad_output_buffer[j]));
            }
            speeches->push_back(e);
        }
    }
    return;
}

/*
 * Description:
 *  Subject the buffered audio and video data to AV sync detection.
 *  This is an advanced version of DoAVSync
 * Return vector of SynchronizationEvents found in the media.
 */
std::vector<SynchronizationEvent> LipSync::DoAVSyncADV() {

  /* Assert state */
  assert(IsTimeStampValid(buffered_sample_timestamp));
  assert(buffered_video_ptr != NULL);
  assert(buffered_audio_ptr != NULL);
  assert(buffered_num_frames > 0);
  assert(buffered_num_audio_samples > 0);
  assert(buffered_seconds > 0);

  double fps_supp = ((double)s->video_properties.time_resolution.video_time_scale) /
                    ((double)s->video_properties.time_resolution.video_sample_duration);
  assert(IsFPSSupported(fps_supp));
  assert(IsAudioPropertiesSupported(s->audio_properties));
  assert(IsVideoPropertiesSupported(s->video_properties));

  assert(s->audio_properties.audio_sample_format == AFMT_PCM_s16le ||
         s->audio_properties.audio_sample_format == AFMT_PCM_s16be);
  assert(s->audio_properties.audio_channels == AC_MONO);
  assert(s->audio_properties.audio_sampling_rate == ASR_44100);

  char process_id[100];
#if _WIN32
  sprintf(process_id, "%d", GetCurrentProcessId());
#else
  sprintf(process_id, "%d", getpid());
#endif /* _WIN32 */

  std::string cache_dir = GenerateUniqueFolderName("lipsync.cache.cpp",
                                                   std::string(process_id));
std::string caching = cache_dir;
  caching.insert(1,"/");
  caching.append(buffered_sample_timestamp);
#if _WIN32
  cache_dir = cache_dir + "\\";
#else
  cache_dir = cache_dir + "/";
#endif /* _WIN32 */

  
  /* Buffer decl. */
  Buffer video_buffer;
  Buffer audio_buffer;

  /* Get FPS */
  int fps = ceil((double) s->video_properties.time_resolution.video_time_scale     /
                 (double) s->video_properties.time_resolution.video_sample_duration);

  std::vector<TimestampSpeech>               ts_speeches;
  std::vector<std::vector<TimestampSpeech> > ts_speech_groups;
  std::vector<SynchronizationEvent>          media_sync_events_collection;

  /********************** Create audio buffer for speech  *********************/

  audio_buffer = Buffer(buffered_audio_ptr,
                        GetAudioSampleSize(s->audio_properties) *
                        buffered_num_audio_samples);



  /******************** Audio buffer -> Speech events *************************/

#define CHECK_STATUS(ret_status, err_stat)                            \
  if (ret_status == -5) {                                             \
    std::cerr<<"FATAL ERROR: CLASSIFIERS NOT FOUND"<<std::endl;       \
    exit(-1);                                                         \
  }                                                                   \
                                                                      \
  if (ret_status == -10) {                                            \
    /* Cancel called */                                               \
    DEBUG_INFO("Cancel called : Returning from Speech\n");            \
    return media_sync_events_collection;                              \
  }                                                                   \
                                                                      \
  if (ret_status != 0) {                                              \
    /* Module fail! Abort */                                          \
    ERR_PRINT(err_stat);                                              \
    return media_sync_events_collection;                              \
  }
  
#ifdef LS_PROFILE_TIME
  unsigned long long int speech_start_time = GetTime();
#endif
  int speech_ret_status;
  /*ts_speeches = GetSampledSpeechBits(s,
                                     audio_buffer,
                                     s->audio_properties,
                                     s->video_properties,
                                     networks[0],
                                     caching,
                                     speech_ret_status,
                                     0);*/
  funcVad(audio_buffer,&ts_speeches);
  speech_ret_status = 0 ;
  make_directory(caching.c_str());
  std::ofstream outfile;
  std::string nameStr = caching+std::string("/speech.txt");
  outfile.open(nameStr.c_str());

  for (const auto &e : ts_speeches) outfile << e.timestamp <<"," <<e.speech_bits << "\n";

  outfile.close();
#ifdef LS_PROFILE_TIME
  double speech_total_time = TimeConsumed(speech_start_time);
  assert(WriteTimeToFile(speech_total_time, "./dump/Speech_detect.txt") == true);
#endif

  DEBUG_INFO("Speech ret val : %d\n", speech_ret_status);
  CHECK_STATUS(speech_ret_status, "speech fail");

  if (!IsTimestampSpeechValid(ts_speeches)) {
    /* speech failed to produce a valid TimestampSpeech vector */
    ERR_PRINT("Speech fail to produce valid TimestampSpeech")
    return media_sync_events_collection;
  }

#undef CHECK_STATUS

  /* update progress */
  progress = 30;

  /**************** Speech Events -> Grouped speech events ********************/

  ts_speech_groups = GroupTimestampSpeeches(ts_speeches, fps);

  /* Sort all timestamp speech groups */
  std::vector<std::vector<TimestampSpeech> >::iterator ts_speech_group;
  for (ts_speech_group  = ts_speech_groups.begin();
       ts_speech_group != ts_speech_groups.end();
       ts_speech_group++) {
    *ts_speech_group = Sort(*ts_speech_group);
  }

  /************ Create video buffer for face/trackface/lip/lipmvt  ************/

  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {

    /* Convert YUV420 to RGB */
    void*  RGB_buffer_ptr   = NULL;
    size_t RGB_buffer_size  = GetFrameSize(s->video_properties) *
                              buffered_num_frames               *
                              2;

    RGB_buffer_ptr = (void *)calloc(RGB_buffer_size,
                                    sizeof(unsigned char));
    assert(RGB_buffer_ptr != NULL && "Cannot allocate memory");

    convert_YUV_to_RGB((unsigned char*)buffered_video_ptr,
                       (unsigned char*)RGB_buffer_ptr,
                       s->video_properties.frame_width,
                       s->video_properties.frame_height,
                       buffered_num_frames);

    /* Assign video raw data (RGB) and its size to Buffer video */
    video_buffer = Buffer(RGB_buffer_ptr,
                          RGB_buffer_size);

  } else if (s->video_properties.pix_fmt == PF_RGB){

    video_buffer = Buffer(buffered_video_ptr,
                          GetFrameSize(s->video_properties) *
                          buffered_num_frames);
  }

  /******************* Process TimestampSpeech group by group *****************/

#define CHECK_STATUS(ret_status, err_stat)                                \
  if (ret_status == -5) {                                                 \
    std::cerr<<"FATAL ERROR: CLASSIFIERS NOT FOUND"<<std::endl;           \
    exit(-1);                                                             \
  }                                                                       \
                                                                          \
  if (ret_status == -10) {                                                \
    /* Cancel called */                                                   \
    if (err_stat == "face fail") {                                        \
      DEBUG_INFO("Cancel called : Returning from Face\n");                \
    } else if (err_stat == "trackface fail") {                            \
      DEBUG_INFO("Cancel called : Returning from TrackFace\n");           \
    } else if (err_stat == "lip fail") {                                  \
      DEBUG_INFO("Cancel called : Returning from Lip\n");                 \
    } else if (err_stat == "lipmovement fail") {                          \
      DEBUG_INFO("Cancel called : Returning from LipMovement\n");         \
    } else if (err_stat == "correlate fail") {                            \
      DEBUG_INFO("Cancel called : Returning from Correlate\n");           \
    } else {                                                              \
    }                                                                     \
                                                                          \
    return media_sync_events_collection;                                  \
  }                                                                       \
                                                                          \
  if (ret_status != 0) {                                                  \
    /* Free video buffer */                                               \
    if (s->video_properties.pix_fmt == PF_I420 ||                         \
        s->video_properties.pix_fmt == PF_IYUV) {                         \
                                                                          \
      free(video_buffer.buffer_data);                                     \
      video_buffer.buffer_data = NULL;                                    \
      video_buffer.buffer_size = 0;                                       \
    }                                                                     \
                                                                          \
    /* Module fail! Abort */                                              \
    ERR_PRINT(err_stat);                                                  \
    return media_sync_events_collection;                                  \
  }

  /* Variables for progress update */
  double progress_dbl = progress;
  double progress_update_step = double(70) / double(ts_speech_groups.size());


  /* Now process timestamps in groups */
  for (ts_speech_group  = ts_speech_groups.begin();
       ts_speech_group != ts_speech_groups.end();
       ts_speech_group++) {

    std::vector<std::string>               timestamps;
    std::vector<TimestampFace>             ts_faces;
    std::vector<TimestampLip>              ts_lips;
    std::vector<TimestampLipMovement>      ts_lipmvts;
    std::vector<SynchronizationEvent>      media_sync_events;

    /********************* TimestampSpeech group to Timestamp *****************/

    std::vector<TimestampSpeech>::iterator ts_speech;
    for (ts_speech  = ts_speech_group->begin();
         ts_speech != ts_speech_group->end();
         ts_speech++) {

      timestamps.push_back(ts_speech->timestamp);
    }

    /***************** Timestamps -> Faces in Timestamps **********************/
#ifdef LS_PROFILE_TIME
  unsigned long long int face_start_time = GetTime();
#endif
    int face_ret_status = 0;
    ts_faces = GetTimestampFaces(s,
                                 timestamps,
                                 video_buffer,
                                 s->video_properties,
                                 networks[1],
                                 caching,
                                 face_ret_status);
#ifdef LS_PROFILE_TIME
  double face_total_time = TimeConsumed(face_start_time);
  assert(WriteTimeToFile(face_total_time, "./dump/Face_detect.txt") == true);
#endif
    
    DEBUG_INFO("Face ret val : %d\n", face_ret_status);
    CHECK_STATUS(face_ret_status, "face fail")

    if (ValidateTimestampFaceList(ts_faces, fps) != 0) {
      /* face failed to produce a valid TimestampFace vector */
      ERR_PRINT("Face fail to produce valid TimestampFace")
      return media_sync_events_collection;
    }

    /*********** Face in Timestamps -> Tracked Faces in Timestamps ************/
#ifdef LS_PROFILE_TIME
  unsigned long long int track_face_start_time = GetTime();
#endif
    int trackface_ret_status = 0;
    ts_faces = TrackFace(s,
                         ts_faces,
                         video_buffer,
                         s -> video_properties,
                         caching,
                         trackface_ret_status);
#ifdef LS_PROFILE_TIME
  double track_face_total_time = TimeConsumed(track_face_start_time);
  assert(WriteTimeToFile(track_face_total_time, "./dump/track_face.txt") == true);
#endif
    DEBUG_INFO("TrackFace ret val : %d\n", trackface_ret_status);
    CHECK_STATUS(trackface_ret_status, "trackface fail")

    if (ValidateTimestampFaceList(ts_faces, fps) != 0) {
      /* trackface failed to produce a valid TimestampFace vector */
      ERR_PRINT("TrackFace fail to produce valid TimestampFace")
      return media_sync_events_collection;
    }

    /************ Trackedfaces in Timestamps -> Lips in Timestamps ************/
#ifdef LS_PROFILE_TIME
  unsigned long long int lip_start_time = GetTime();
#endif
    int lip_ret_status = 0;
    ts_lips = GetTimestampLips(s,
                               ts_faces,
                               video_buffer,
                               s->video_properties,
                               networks[2],
                               caching,
                               &lip_ret_status);
#ifdef LS_PROFILE_TIME
  double lip_total_time = TimeConsumed(lip_start_time);
  assert(WriteTimeToFile(lip_total_time, "./dump/lip_detect.txt") == true);
#endif
    DEBUG_INFO("Lip ret val : %d\n", lip_ret_status);
    CHECK_STATUS(lip_ret_status, "lip fail")

    if (!IsTimestampLipValid(ts_lips)) {
      /* lip failed to produce a valid TimestampLip */
      ERR_PRINT("Lip fail to produce valid TimestampLip")
      return media_sync_events_collection;
    }

    /********** Lips in Timestamps to LipMovements in Timestamps **************/
#ifdef LS_PROFILE_TIME
  unsigned long long int lip_mov_start_time = GetTime();
#endif
    int lipmovement_ret_status = 0;
    ts_lipmvts = GetTimestampLipMovements(s,
                                          ts_lips,
                                          ts_faces,
                                          video_buffer,
                                          s->video_properties,
                                          networks[3],
                                          caching,
                                          lipmovement_ret_status);
#ifdef LS_PROFILE_TIME
  double lip_mov_total_time = TimeConsumed(lip_mov_start_time);
  assert(WriteTimeToFile(lip_mov_total_time, "./dump/lip_mov_detect.txt") == true);
#endif
    DEBUG_INFO("LipMovement ret val : %d\n", lipmovement_ret_status);
    CHECK_STATUS(lipmovement_ret_status, "lipmovement fail")

    if (!IsTimestampLipMovementValid(ts_lipmvts)) {
      /* LipMovement failed to produce a valid TimestampLipMovement vector */
      ERR_PRINT("LipMovement fail to produce valid TimestampLipMovement")
      return media_sync_events_collection;
    }

    /** Correlate current TimestampSpeech group and its TimestampLipMovement **/

    int correlate_ret_status = 0;
    media_sync_events = correlate::Correlate(s,
                                             *ts_speech_group,
                                             ts_lipmvts,
                                             s->video_properties,
                                             buffered_sample_timestamp,
                                             buffered_seconds,
                                             buffered_sample_uniqueid,
                                             correlate_ret_status);

    DEBUG_INFO("Correlate ret val : %d\n", correlate_ret_status);
    CHECK_STATUS(correlate_ret_status, "correlate fail");

    /********************** Do Callbacks and Update Stream ********************/

    /* Update media_sync_events_collection */
    std::vector<SynchronizationEvent>::iterator sync_event;
    for (sync_event  = media_sync_events.begin();
         sync_event != media_sync_events.end();
         sync_event++) {
      media_sync_events_collection.push_back(*sync_event);
    }

    /* Update progress */
    progress_dbl += progress_update_step;
    if (ceil(progress_dbl) < 100) {
      progress = ceil(progress_dbl);
    }
  }

  /* Video buffer allocation done when YUV -> RGB */
  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {

    free(video_buffer.buffer_data);
    video_buffer.buffer_data = NULL;
    video_buffer.buffer_size = 0;
  }

#undef CHECK_STATUS

  /* Make a copy */
  std::vector<SynchronizationEvent> media_sync_events;
  media_sync_events = media_sync_events_collection;

  /* Call DoCallbacks */
  DoCallbacks(media_sync_events);

  /* Update Stream */
  UpdateStreamSynchronizationEvents(media_sync_events_collection);
  return media_sync_events_collection;
}

/*
 * Description:
 *  Subject the buffered audio and video data to AV sync detection.
 *
 * Return vector of SynchronizationEvents found in the media
 */
std::vector<SynchronizationEvent> LipSync::DoAVSync() {

  /* Assert state */
  assert(IsTimeStampValid(buffered_sample_timestamp));
  assert(buffered_video_ptr != NULL);
  assert(buffered_audio_ptr != NULL);
  assert(buffered_num_frames > 0);
  assert(buffered_num_audio_samples > 0);
  assert(buffered_seconds > 0);

  std::vector<SynchronizationEvent> media_sync_events;

  double fps = ((double)s->video_properties.time_resolution.video_time_scale) /
                            ((double)s->video_properties.time_resolution.video_sample_duration);
  assert(IsFPSSupported(fps));
  assert(IsAudioPropertiesSupported(s->audio_properties));
  assert(IsVideoPropertiesSupported(s->video_properties));

  assert(s->audio_properties.audio_sample_format == AFMT_PCM_s16le ||
         s->audio_properties.audio_sample_format == AFMT_PCM_s16be);
  assert(s->audio_properties.audio_channels == AC_MONO);
  assert(s->audio_properties.audio_sampling_rate == ASR_44100);

  char process_id[100];
#if _WIN32
  sprintf(process_id, "%d", GetCurrentProcessId());
#else
  sprintf(process_id, "%d", getpid());
#endif /* _WIN32 */

  std::string cache_dir = GenerateUniqueFolderName("lipsync.cache.cpp",
                                                   std::string(process_id));

#if _WIN32
  cache_dir = cache_dir + "\\";
#else
  cache_dir = cache_dir + "/";
#endif /* _WIN32 */

  std::cout<<buffered_sample_uniqueid<<" : "<<"cache folder - "<<cache_dir<<std::endl;

  /******************************* SPEECH DETECT *******************************/

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned  long long speech_start = GetTime();
#endif
#endif

  int speechdetect_ret_val_cpp;
  speechdetect_ret_val_cpp = ProcessSpeech(buffered_audio_ptr,
                                           GetAudioSampleSize(s->audio_properties) *
                                                       buffered_num_audio_samples,
                                           fps,
                                        s->audio_properties.audio_sample_format,
                                           cache_dir.c_str(),
                                           classifiers_path,
                                           0);

  if (speechdetect_ret_val_cpp == 5) {
    std::cerr<<"FATAL ERROR: CLASSIFIERS NOT FOUND"<<std::endl;
    exit(-1);
  }

  /* Run DAR-GetSampledSpeechBits and check for output correctness */
  bool dar_speech_test_pass = test_GetSampledSpeechBits(
                                       s,
                                       speechdetect_ret_val_cpp,
                                       buffered_audio_ptr,
                                       GetAudioSampleSize(s->audio_properties) *
                                                   buffered_num_audio_samples,
                                       s->audio_properties,
                                       s->video_properties,
                                       networks[0],
                                       cache_dir);

  if (!dar_speech_test_pass) {
    std::cout<<buffered_sample_uniqueid<<" : "<<
                          "speech fail"<<std::endl;
  } else {
    //std::cerr<<buffered_sample_uniqueid<<
    //                      " GetSampledSpeechBits == ProcessSpeech"<<std::endl;
  }

  if (speechdetect_ret_val_cpp != 0) {
    std::cout<<buffered_sample_uniqueid<<" : "<<
                                          "Classic speech failed !"<<std::endl;
    return media_sync_events;
  }

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double speech_time = TimeConsumed(speech_start);
#endif
#endif

  /*****************************************************************************/

  /* Update progress */
  progress = 20;

  /******************************* FACE DETECT *********************************/

  int facedetect_ret_val_cpp;

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned  long long face_start = GetTime();
#endif
#endif

#if REMOVE_DISK_ACCESS_FACE
  /*
   * Temporary:
   * Get timestamps list using GetInputForFace() which was input
   * for GetTimestampFaces()
   */
  std::vector<std::string> timestamps = GetInputForFace(cache_dir);

  /* If cache_folder needs to dumped, set directory name.
   * Otherwise set empty string
   */
  std::string temp_cache_dir = GenerateUniqueFolderName("lipsync.cache.face.DAR",
                                                        std::string(process_id));
  /* std::string temp_cache_dir = ""; */

  /* Append SPLIT_CHAR if cache directory is not empty string */
#if _WIN32
  if (temp_cache_dir != "") {
    temp_cache_dir = temp_cache_dir + "\\";
  }
#else
  if (temp_cache_dir != "") {
    temp_cache_dir = temp_cache_dir + "/";
  }
#endif /* _WIN32 */

  Buffer video;

  /* YUV420 support */
  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {

    /* Convert YUV420 to RGB */
    void* RGB_video_ptr = NULL;
    RGB_video_ptr = (void *)calloc(GetFrameSize(s->video_properties) *
                                   buffered_num_frames * 2, sizeof(unsigned char));
    convert_YUV_to_RGB((unsigned char*)buffered_video_ptr,
                       (unsigned char*)RGB_video_ptr,
                       s->video_properties.frame_width,
                       s->video_properties.frame_height,
                       buffered_num_frames);

    /* Assign video raw data (RGB) and its size to Buffer video */
    video.buffer_data = RGB_video_ptr;
    video.buffer_size = GetFrameSize(s->video_properties) * buffered_num_frames * 2;
  }

  /* RGB support */
  if (s->video_properties.pix_fmt == PF_RGB) {
    /* Assign video raw data and its size to Buffer video */
    video.buffer_data = buffered_video_ptr;
    video.buffer_size = GetFrameSize(s->video_properties) * buffered_num_frames;
  }


  /* call GetTimestampFaces() */
  int face_ret_val = -99;
  std::vector<TimestampFace> timestamp_face_list = GetTimestampFaces(s,
                                                                     timestamps,
                                                                     video,
                                                                     s -> video_properties,
                                                                     networks[1],
                                                                     temp_cache_dir,
                                                                     face_ret_val);
  //std::cout << "[DAR] Return value (Face Module):" << face_ret_val << std::endl;

#endif // REMOVE_DISK_ACCESS_FACE

  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {

    void* RGB_video_ptr = NULL;
    RGB_video_ptr = (void *)calloc(GetFrameSize(s->video_properties) *
                                   buffered_num_frames * 2, sizeof(unsigned char));
    convert_YUV_to_RGB((unsigned char*)buffered_video_ptr,
                       (unsigned char*)RGB_video_ptr,
                       s->video_properties.frame_width,
                       s->video_properties.frame_height,
                       buffered_num_frames);
    facedetect_ret_val_cpp = process_face_main(
                                         RGB_video_ptr,
                                         GetFrameSize(s->video_properties) *
                                         buffered_num_frames * 2,
                                         fps,
                                         s->video_properties.frame_height,
                                         s->video_properties.frame_width,
                                         cache_dir.c_str(),
                                         classifiers_path);
    free(RGB_video_ptr);

  } else {

    // RGB support
    facedetect_ret_val_cpp = process_face_main(
                                         buffered_video_ptr,
                                         GetFrameSize(s->video_properties) *
                                                         buffered_num_frames,
                                         fps,
                                         s->video_properties.frame_height,
                                         s->video_properties.frame_width,
                                         cache_dir.c_str(),
                                         classifiers_path);
  }

#if REMOVE_DISK_ACCESS_FACE
  /* Compare Functions */
  bool compare_face = test_GetTimestampFaces(cache_dir,
                                            timestamp_face_list,
                                            video,
                                            s->video_properties);

  if (!compare_face) {
    std::cout <<buffered_sample_uniqueid<<" : "<<
                                      "face fail" << std::endl;
    std::string save_cache_name = buffered_sample_uniqueid + "_face/";
    std::string cmd_cp = "cp -r " + temp_cache_dir + " " + save_cache_name;
    system(cmd_cp.c_str());
  }
#endif // REMOVE_DISK_ACCESS_FACE

  /* facedetect Could not find classifiers. FATAL */
  if (facedetect_ret_val_cpp == 5) {
    std::cerr << "FATAL ERROR: CLASSIFIERS NOT FOUND" << std::endl;
    exit(-1);
  }


#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double face_time = TimeConsumed(face_start);
#endif
#endif

  /*****************************************************************************/

  /* Update progress */
  progress = 30;

  /******************************* TRACK FACE **********************************/
#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned long long trackface_start = GetTime();
#endif
#endif

#if REMOVE_DISK_ACCESS_TRACKFACE
#if REMOVE_DISK_ACCESS_FACE
  std::vector< std::vector<std::string> > timestamp_grp_list =
                                             GroupTimestamps(timestamps, int(fps + 0.5));

  /* Get Number of groups */
  unsigned int group_count = timestamp_grp_list.size();

  unsigned int timestamp_list_count = 0;

  std::vector<TimestampFace> timestamp_trackface_list;

  /* Iterate every group to TrackFace() */
  for(unsigned int grp_iter = 0;
      grp_iter < group_count;
      grp_iter ++) {

    /* Get number of timestamps from each group */
    unsigned int grp_timestamp_count = timestamp_grp_list[grp_iter].size();

#if LS_DEBUG
    std::cout << "Group                  = " << grp_iter << std::endl;
    std::cout << "Number of timestamps   = " << grp_timestamp_count << std::endl;
#endif // LS_DEBUG

    /* Timestamp face list for every group */
    std::vector<TimestampFace> timestamp_face_grp_list;

    /* Iterate to get group of TimeStampFace object */
    for (unsigned int timestamp_iter = timestamp_list_count;
         timestamp_iter < (timestamp_list_count + grp_timestamp_count);
         timestamp_iter ++) {

#if LS_DEBUG
      std::cout << timestamp_face_list[timestamp_iter].timestamp << std::endl;
#endif // LS_DEBUG

      timestamp_face_grp_list.push_back(timestamp_face_list[timestamp_iter]);
    }

#if LS_DEBUG
    std::cout << "Timestamp start number = " << timestamp_list_count << std::endl;
    std::cout << std::endl;
#endif // LS_DEBUG

    int trackface_ret_val = -99;
    /* Apply TrackFace() for each face list */
    timestamp_face_grp_list = TrackFace(s,
                                        timestamp_face_grp_list,
                                        video,
                                        s -> video_properties,
                                        temp_cache_dir,
                                        trackface_ret_val);
    //std::cout << "[DAR] Return value (TRACK FACE Module):" << trackface_ret_val << std::endl;

    /* Iterate to add TimeStampFace object */
    for (unsigned int timestamp_iter = 0;
         timestamp_iter < timestamp_face_grp_list.size();
         timestamp_iter ++) {
      timestamp_trackface_list.push_back(timestamp_face_grp_list[timestamp_iter]);
    }

    timestamp_list_count += grp_timestamp_count;
  }

#endif // REMOVE_DISK_ACCESS_FACE
#endif // REMOVE_DISK_ACCESS_TRACKFACE

  int trackface_ret_val_cpp;
  trackface_ret_val_cpp = ProcessTrackFace(fps, cache_dir.c_str());

#if REMOVE_DISK_ACCESS_TRACKFACE
  /* Compare Functions */
  bool compare_track_face = test_TrackFace(cache_dir,
                                           timestamp_trackface_list,
                                           video,
                                           s->video_properties);

  if (!compare_track_face) {
    std::cout <<buffered_sample_uniqueid<<" : "<<
                                       "trackface fail"<< std::endl;
    if (!compare_face) {
      std::string save_cache_name = buffered_sample_uniqueid + "_trackface/";
      std::string cmd_cp = "cp -r " + temp_cache_dir + " " + save_cache_name;
      system(cmd_cp.c_str());
    }
  }
#endif // REMOVE_DISK_ACCESS_TRACKFACE


#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double trackface_time = TimeConsumed(trackface_start);
#endif
#endif
  /*****************************************************************************/

  /* Update progress */
  progress = 40;

  /******************************* LIP DETECT **********************************/
  /* Default Lip Module */
#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned  long long lip_start = GetTime();
#endif
#endif

  int lipdetect_ret_val_cpp;
  lipdetect_ret_val_cpp =  ProcessLip(fps, cache_dir.c_str(), classifiers_path);

  // lipdetect could not find classifiers. FATAL
  if (lipdetect_ret_val_cpp == 5) {
    std::cerr << "FATAL ERROR: CLASSIFIERS NOT FOUND" << std::endl;
    exit(-1);
  }


#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double lip_time = TimeConsumed(lip_start);
#endif
#endif

/*****************************************************************************/
  /* Disk Access Removal - Lip Module Input/Output generation */

  int ret_status;

  /* gets the list of face coordinates and probablities for all detected faces
   in all timestamps into timestamp_face_list */
  std::vector<TimestampFace>  timestamp_face_list_temp = GenerateLipInput(
    fps,
    cache_dir,
    &ret_status);

#if LS_DEBUG
  std::cout<<"Default Timestamp Face list"<<std::endl;
  for (int i = 0; i < timestamp_face_list_temp.size(); ++i)
    PrintTimestampFace(timestamp_face_list_temp[i]);
#endif // LS_DEBUG

  /* gets the list of lip coordinates and probablities for all detected lips
   in all timestamps into timestamp_lip_list */
  std::vector<TimestampLip>  correct_timestamp_lip_list = GenerateLipMvtInput(
    fps,
    cache_dir,
    &ret_status);

/*****************************************************************************/
/* Disk Access Removal - Lip Module */

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned  long long DAR_lip_start = GetTime();
#endif
#endif

  int return_status = 0;
  std::vector<TimestampLip> timestamp_lip_list;
  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {

    /* RGB_video_ptr => video data converted from YUV to RGB */
    void* RGB_video_ptr = NULL;
    RGB_video_ptr = (void *)calloc(GetFrameSize(s->video_properties) *
                                   buffered_num_frames * 2, sizeof(unsigned char));
    convert_YUV_to_RGB((unsigned char*)buffered_video_ptr,
                       (unsigned char*)RGB_video_ptr,
                       s->video_properties.frame_width,
                       s->video_properties.frame_height,
                       buffered_num_frames);

    Buffer video(RGB_video_ptr, GetFrameSize(s->video_properties) *
                                         buffered_num_frames * 2);

    /* gets the list of lip coordinates and probablities for all detected lips
     in all timestamps into timestamp_lip_list */
     timestamp_lip_list = GetTimestampLips(
      s,
      timestamp_face_list_temp,
      video,
      s->video_properties,
      networks[2],
      temp_cache_dir.c_str(),
      &return_status);

    free(RGB_video_ptr);
} else {
    Buffer video(buffered_video_ptr, GetFrameSize(s->video_properties) *
                                         buffered_num_frames);

    timestamp_lip_list = GetTimestampLips(
      s,
      timestamp_face_list_temp,
      video,
      s->video_properties,
      networks[2],
      temp_cache_dir.c_str(),
      &return_status);
}

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double DAR_lip_time = TimeConsumed(DAR_lip_start);
#endif
#endif

 /*****************************************************************************/
 /* Compare Default Lip Module and Disk Access Removal Lip Module */

  if (!CompareTimestampLip(correct_timestamp_lip_list, timestamp_lip_list)) {
    std::cout<<"DEFAULT LIP OUTPUT:"<<std::endl;
    for (int i = 0; i < int(correct_timestamp_lip_list.size()); ++i)
      PrintTimestampLip(correct_timestamp_lip_list[i]);
    std::cout<<"--------------------------------"<<std::endl;
    std::cout<<"DAR LIP OUTPUT:"<<std::endl;
    for (int i = 0; i < int(timestamp_lip_list.size()); ++i)
      PrintTimestampLip(timestamp_lip_list[i]);
    std::cout <<buffered_sample_uniqueid<<" : "<< "lip fail" << std::endl;

    /* Save cache */
    std::string save_cache_name = buffered_sample_uniqueid + "_lip/";
    std::string cmd_cp = "cp -r " + temp_cache_dir + " " + save_cache_name;
    system(cmd_cp.c_str());
  }

  /* Update progress */
  progress = 60;

  /********************************** LIP MOVEMENT *****************************/
#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  unsigned  long long lipmvt_start = GetTime();
#endif
#endif

  /**********************  Store inputs for dar lipmovement ********************/

#if _WIN32
  #define SPLIT_CHAR '\\'
#else
  #define SPLIT_CHAR '/'
#endif /* _WIN32 */

#define LAST_INDEX(s) (s.length() - 1)
#define LAST_CHAR(s) (s[s.length() - 1])
  /* Create dump cache folder */
  std::string dump_cache_folder;

#if _WIN32
  if (LAST_CHAR(cache_dir) == SPLIT_CHAR)
#else
  if (LAST_CHAR(cache_dir) == SPLIT_CHAR)
#endif
  {
     dump_cache_folder = cache_dir;
     dump_cache_folder[LAST_INDEX(cache_dir)] = '_';
     dump_cache_folder += "_dar";
  } else {
    dump_cache_folder =  cache_dir + "_dar";
  }

  /* Set up dump cache folder */
  std::string copy_cmd = "cp -r " + cache_dir + " " +
                         dump_cache_folder;
  system(copy_cmd.c_str());
  //std::cout<<"dump cache folder "<<dump_cache_folder<<std::endl;

  std::vector<std::string> of_folders_list =  PatternSearch( dump_cache_folder +
                SPLIT_CHAR + "?-??-??.???" + SPLIT_CHAR + "optical_flow_lip_*");

  std::vector<std::string>::iterator of_folder;
  for (of_folder  = of_folders_list.begin();
       of_folder != of_folders_list.end();
       of_folder++) {

    std::string cmd_rm = "rm -rf " + *of_folder;

    system(cmd_rm.c_str());

  }

  std::vector<std::string> sample_folders_list =  PatternSearch( dump_cache_folder +
                SPLIT_CHAR + "?-??-??.???" + SPLIT_CHAR + "lip_*" + SPLIT_CHAR +
                "Sample_*");

  std::vector<std::string>::iterator sample_folder;
  for (sample_folder  = sample_folders_list.begin();
       sample_folder != sample_folders_list.end();
       sample_folder++) {

    std::string cmd_rm = "rm -rf " + *sample_folder;

    system(cmd_rm.c_str());

  }

#undef LAST_CHAR
#undef LAST_INDEX
#undef SPLIT_CHAR

  /******************************* Classic lipmovement ************************/
  int lipmvt_ret_val_cpp;
  lipmvt_ret_val_cpp = ProcessLipMovement(fps, cache_dir.c_str(), classifiers_path);

  /* lipmovement could not find classifiers! FATAL */
  if (lipmvt_ret_val_cpp == 5) {
    std::cerr << "FATAL ERROR: CLASSIFIERS NOT FOUND" << std::endl;
    exit(-1);
  }

  /******************************* DAR lipmovemetn ****************************/
  bool dar_lipmovement_passed = dar_lipmovement_test(
                                         s,
                                         cache_dir,
                                         dump_cache_folder,
                                         buffered_video_ptr,
                                         GetFrameSize(s->video_properties) *
                                                         buffered_num_frames,
                                         s->video_properties,
                                         networks[3]);

  if (!dar_lipmovement_passed) {
    std::cout<<buffered_sample_uniqueid<<" : "<<"lipmovement fail"<<std::endl;

    std::string save_cache_name = buffered_sample_uniqueid + "_lipmvt/";
    std::string cmd_cp = "cp -r " + dump_cache_folder + " " + save_cache_name;
    system(cmd_cp.c_str());
  }

  if (lipmvt_ret_val_cpp != 0) {
    std::cout<<buffered_sample_uniqueid<<" : "<<
                                        "Classic lipmovement failed"<<std::endl;
    return media_sync_events;
  }

  /* delete dar cache folder */
  std::string cmd_rm = "rm -rf " + dump_cache_folder;
  system(cmd_rm.c_str());

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double lipmvt_time = TimeConsumed(lipmvt_start);
#endif
#endif
  /*****************************************************************************/

  /* Update progress */
  progress = 80;

  /******************************** HISTOGRAM **********************************/

  /* Run histogram */
  std::vector<std::pair<std::string, int> > event_skew_pairs; // CPP output

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
 unsigned  long long hist_start = GetTime();
#endif
#endif

    event_skew_pairs = ProcessHistogramMain(cache_dir,
                                            buffered_sample_timestamp,
                                            fps,
                                            buffered_seconds);

#ifdef LS_DEBUG
#ifdef LS_PROFILE_TIME
  double hist_time = TimeConsumed(hist_start);
#endif
#endif

#ifdef LS_DEBUG
#if LS_PROFILE_TIME
  double all_mod_time = speech_time +
                        face_time +
                        lip_time +
                        lipmvt_time +
                        hist_time;

  printf("---TIMINGS---\n");
  printf("speech      : %lf secs\n", speech_time);
  printf("face        : %lf secs\n", face_time);
  printf("trackface   : %lf secs\n", trackface_time);
  printf("lip         : %lf secs\n", lip_time);
  printf("lipmvt      : %lf secs\n", lipmvt_time);
  printf("histogram   : %lf secs\n", hist_time);
  printf("All modules : %lf secs\n", all_mod_time);
  printf("---TIMINGS---\n");

#endif
#endif // LS_DEBUG

  media_sync_events = InterpretEventResults(event_skew_pairs,
                                            buffered_sample_uniqueid,
                                            fps,
                                            buffered_sample_timestamp);

  bool correlate_pass = test_Correlate(s,
                                       cache_dir,
                                       s->video_properties,
                                       buffered_sample_timestamp,
                                       buffered_seconds,
                                       buffered_sample_uniqueid,
                                       media_sync_events);
  if (correlate_pass) {
    //std::cout<<"Correlate test passed"<<std::endl;
  } else {
    std::cout<<buffered_sample_uniqueid<<" : "<<"correlate fail"<<std::endl;

    std::string save_cache_name = buffered_sample_uniqueid + "_correlate/";
    std::string cmd_cp = "cp -r " + cache_dir + " " + save_cache_name;
    system(cmd_cp.c_str());
  }

  /* Update progress */
  progress = 90;

#if REMOVE_DISK_ACCESS_FACE
#if _WIN32
  system(std::string("rmdir /s /q " + temp_cache_dir).c_str());
#else
  system(std::string("rm -rf " + temp_cache_dir).c_str());
#endif // _WIN32
#endif // REMOVE_DISK_ACCESS_FACE

#if _WIN32
  system(std::string("rmdir /s /q " + cache_dir).c_str());
#else
  system(std::string("rm -rf " + cache_dir).c_str());
#endif

#if REMOVE_DISK_ACCESS_FACE
  /*
   * For YUV420 Pixel format, video raw format was converted to
   * RGB format (For face-detect). RGB video raw memory need to
   * be released at last.
   */
  if (s->video_properties.pix_fmt == PF_I420 ||
      s->video_properties.pix_fmt == PF_IYUV) {
    FreeBuffer(video);
  }
#endif // REMOVE_DISK_ACCESS_FACE

  /* Do callbacks */
  DoCallbacks(media_sync_events);

  /* Update Stream */
  UpdateStreamSynchronizationEvents(media_sync_events);

  return media_sync_events;
}

/* Argument 1 : video             - Pointer to video frames adhereing to the
 *                                  Stream's video properties struct.
 * Argument 2 : audio             - Pointer to audio samples adhereing to the
 *                                  Stream's audio properties struct.
 * Argument 3 : num_frames        - Size of the sample in '# frames' contained
 *                                  in argument-1 (void *video). If 'num_frames'
 *                                  is 2, and if the Stream's VideoProperties
 *                                  read-out as follows,
 *                                   frame dimension - 100X100,
 *                                   frame pixel fmt - PF_RGB,
 *                                   frame bit depth - 8,
 *                                  then the argument-1(*video) is expected to be of
 *                                  size 'W * H * bit-depth * #color_channels * #frames'
 *                                  (100 * 100 * 8 * 3 * 2) bits (may change for other
 *                                  PixelFormat).
 * Argument 4 : num_audio_samples - Number of samples representing audio.
 *                                  If 'num_audio_samples' is 33057 and if the
 *                                  Stream's AudioProperties read-out as follows,
 *                                   audio_sample_format - AFMT_PCM_s16le,
 *                                   audio_channels      - AC_STEREO_INTERLEAVED,
 *                                   audio_sample_format - ASR_44100,
 *                                  then argument-2 (void *audio) is expected
 *                                  to be of size 'size-of-1-sample-in-bits * #audio_channels * num_audio_samples'
 *                                  (16 * 2 * 33057) bits.
 * Argument 5 : time_stamp        - time_stamp is a string of format "HHHHH:MM:SS.SSS"
 *                                  where 'HHHHH' is hours, 'MM' is minutes,
 *                                  'SS.SSS' is seconds in milli seconds. The
 *                                  time_stamp denotes the start time to be
 *                                  associated with the start of the sample.
 *                                  ProcessSample() buffers data internally untill
 *                                  it has enough data to run sync-detection.
 *                                  Thus is may take multiple ProcessSample()
 *                                  calls before a sync-detection is performed.
 *                                  In such a situation, the time_stamp of the
 *                                  very first ProcessSample() call, w.r.t. data
 *                                  currently buffered, is considered; All other
 *                                  'time_stamp' values are discarded.
 * Argument 6 : unique_id         - Unique id to identify the input sample in
 *                                  the logs. This 'unique_id' can be any string
 *                                  that the user sees fit. Like Argument 5, Only
 *                                  the 'unique_id' of the very first ProcessSample()
 *                                  call, w.r.t. data currently buffered is
 *                                  considered; All other 'unique_id' values are
 *                                  discarded.
 * Description :
 *   ProcessSample() needs sufficient video and audio data (critical volume)
 *  to run sync detection. It buffers data internally until the data accumulates
 *  to reach the critical volume.
 *  When submitted with input data 'X', ProcessSample() determines
 *    if 'X' + 'internally-buffered-data'  > critical-volume,
 *      if yes - 'X' is appended to 'internally-buffered-data' and sync-detection
 *                is run on the data.
 *      if No  - 'X' is appended to 'internally-buffered-data' and ProcessSample
 *                returns immediately.
 *   ProcessSample() also logs the sample's sync-detection information, that
 *  can be queried using the GetSummary(), GetSyncEvents() methods found in
 *  the associated Stream object. ProcessSample() invokes user callback set
 *  using Stream::SetSyncCallback(), if any.
 *   ProcessSample expects that the video and audio pointers passed, contain video
 *  and audio data that correspond to exactly the same time frame. For example, If
 *  arg-1 holds frames from exactly the 10th second to 13.243th second, then the
 *  arg-2 is also expected to hold the audio samples from exactly the 10th second to
 *  13.243th second.
 *
 *  Error:
 *   1. LSERR_LIPSYNCOBJ_NOT_CREATED_ERR - Calling LipSync::ProcessSample() when LipSync
 *      is not even Create()'d.
 *   2. LSERR_LIPSYNCOBJ_NOT_ASSOC_ERR - Calling LipSync::ProcessSample() when there
 *      is no Stream associated with LipSync.
 *   3. LSERR_CLASSIFIERS_NOT_FOUND - LipSync could not find classifiers
 *   4. LSERR_INVALID_ARG_ERR - If either of arg-1 or arg-2 is NULL.
 *
 * Returns LSERR_SUCCESS on success.
 */
LSError_t LipSync::ProcessSample(void *video,
                                 void *audio,
                                 unsigned int num_frames,
                                 unsigned int num_audio_samples,
                                 std::string time_stamp,
                                 std::string unique_id) {

  bool mutex_lock_acquired;
  bool mutex_lock_released;
  /* Lock mutex; Mark ProcessSample start execution */
  mutex_lock_acquired = LSAcquireMutexLock(&process_sample_mutex);
  assert(mutex_lock_acquired && "Cannot acquire mutex lock");

  if(!is_created) {
    /* LipSync object not created */

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_LIPSYNCOBJ_NOT_CREATED_ERR;
  }
  if(!IsStreamAssociated()) {
    /* Not associated with any Stream */

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_LIPSYNCOBJ_NOT_ASSOC_ERR;
  }

  /* TODO: Need to add more comments */
  if (s->IsCancelStateSet()) {

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_STREAMOBJ_CANCEL_PROCESS_INITIATED;
  }

  if(video == NULL || audio == NULL || num_frames == 0 ||
     num_audio_samples == 0 ||
     !IsTimeStampValid(time_stamp)) {

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_INVALID_ARG_ERR;
  }

  /* Check if the classifiers path remains valid */
  if (CheckClassifierPathExists(classifiers_path) == 1) {

    /* Unlock mutex */
    mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
    assert(mutex_lock_released && "Cannot release mutex lock");

    return LSERR_CLASSIFIERS_NOT_FOUND;
  }

  /* Initiate progress */
  progress = 0;

  assert(IsVideoPropertiesSupported(s->video_properties));
  assert(IsAudioPropertiesSupported(s->audio_properties));

  /* Update internal buffer with current sample */
  UpdateBuffer(video, audio, num_frames, num_audio_samples, time_stamp, unique_id);
  if(buffered_seconds >= CRITICAL_VOLUME) {

#if 0
    DoAVSync();
#endif

#if 1
    DoAVSyncADV();
#endif

    ResetBuffer();

  } else {
    /* Return immediately */
  }

  /* Reset progress */
  progress = -1;

  LSError_t do_avsync_result;
  /* TODO: Add comments */
  if (s->IsCancelStateSet()) {
    do_avsync_result = LSERR_STREAMOBJ_CANCEL_PROCESS_INITIATED;
  } else {
    do_avsync_result = LSERR_SUCCESS;
  }

  /* Unlock mutex */
  mutex_lock_released = LSReleaseMutexLock(&process_sample_mutex);
  assert(mutex_lock_released && "Cannot release mutex lock");

  return do_avsync_result;
}

/*
 * Description:
 *  Frees all internal buffer contents and resets all state variables.
 */
void LipSync::ResetBuffer() {

  /* Sanity check */
  if(buffered_video_ptr               != NULL) {
    assert(buffered_audio_ptr         != NULL);
    assert(buffered_num_frames        != 0);
    assert(buffered_num_audio_samples != 0);
    assert(buffered_seconds           != 0);
    assert(buffered_sample_timestamp  != "");
  }
  if(buffered_video_ptr == NULL) {
    assert(buffered_audio_ptr         == NULL);
    assert(buffered_num_frames        == 0);
    assert(buffered_num_audio_samples == 0);
    assert(buffered_seconds           == 0);
    assert(buffered_sample_timestamp  == "");
    assert(buffered_sample_uniqueid   == "");
  }

  if(buffered_video_ptr != NULL) {
    /* Free buffered video pointer */
    free(buffered_video_ptr);
    buffered_video_ptr = NULL;
  }

  if(buffered_audio_ptr != NULL) {
    /* Free buffered audio pointer */
    free(buffered_audio_ptr);
    buffered_audio_ptr         = NULL;
  }

  buffered_num_frames        = 0;
  buffered_num_audio_samples = 0;
  buffered_sample_timestamp  = "";
  buffered_sample_uniqueid   = "";
  buffered_seconds           = 0;

}

/*
 * Arugument 1 - video_prop : A valid supported VideoProperties struct
 *
 * Description :
 *  Given a valid supported VideoProperties struct, return the size in bytes for
 *  single frame.
 *
 * Return : Size of a frame in bytes.
 */
size_t LipSync::GetFrameSize(VideoProperties video_prop) {

  assert(IsVideoPropertiesSupported(video_prop));
  assert(video_prop.pix_fmt == PF_RGB  ||
         video_prop.pix_fmt == PF_IYUV ||
         video_prop.pix_fmt == PF_I420); /* Only supported pixel format */
  assert(video_prop.bit_depth == PBD_8); /* Only supported pixel bit depth */

  if(video_prop.pix_fmt == PF_RGB) {
    if(video_prop.bit_depth == PBD_8) {
      return (video_prop.frame_width * video_prop.frame_height * 3 * 1);
    }
  }

  if (video_prop.pix_fmt == PF_IYUV || video_prop.pix_fmt == PF_I420) {
    if(video_prop.bit_depth == PBD_8) {
      return (video_prop.frame_width * video_prop.frame_height * 1.5 * 1);
    }
  }

  return 0;
}
/*
 * Argument 1 - audio_prop : A valid supported AudioProperties struct.
 *
 * Description :
 *  Given a valid supported AudioProperties struct, return the size in bytes of
 *  a single audio sample.
 *
 * Return : Size of a single audio sample in bytes
 */
size_t LipSync::GetAudioSampleSize(AudioProperties audio_prop) {

  assert(IsAudioPropertiesSupported(audio_prop));
  assert(audio_prop.audio_sample_format == AFMT_PCM_s16le ||
         audio_prop.audio_sample_format == AFMT_PCM_s16be);
  assert(audio_prop.audio_channels == AC_MONO);

  if(audio_prop.audio_sample_format == AFMT_PCM_s16le ||
     audio_prop.audio_sample_format == AFMT_PCM_s16be) {
    if(audio_prop.audio_channels == AC_MONO) {
      return (2 * 1); /* 2 bytes * 1 channel */
    }
    if(audio_prop.audio_channels == AC_STEREO_INTERLEAVED) {
      return (2 * 2); /* 2 bytes * 2 channels */
    }
  }

  return 0;
}

/*
 * Argument 1 - video             : video pointer.
 * Argument 2 - audio             : audio pointer.
 * Argument 3 - num_frames        : Number of frames contained in video ptr.
 * Argument 4 - num_audio_samples : Number of audio samples contained
 *                                  in audio ptr.
 * Argument 5 - time_stamp        : Time stamp to be associated with update.
 * Argument 6 - uniqueid          : Uniqueid to be associated with update.
 *
 * Description :
 *  Append video and audio to buffered_video_ptr and buffered_audio_ptr respectively.
 *  If buffered_video_ptr and buffered_audio_ptr is NULL, then allocate memory and
 *  copy 'video' and 'audio' data to the newly allocated memory. Associate 'time_stamp'
 *  to this buffer.
 *  If buffered_video_ptr and buffered_audio_ptr are not null, meaning that they already
 *  have some buffered content, then append 'video' and 'audio' data to
 *  buffered_video_ptr and buffered_audio_ptr resply. A time stamp would already have been
 *  associated with the buffer, and argument-5 is useless in this case.
 */
void LipSync::UpdateBuffer(void *video, void *audio, unsigned int num_frames,
                         unsigned int num_audio_samples, std::string time_stamp,
                         std::string uniqueid) {

  assert(num_frames != 0);
  assert(num_audio_samples != 0);
  assert(video != NULL); assert(audio != NULL);
  assert(time_stamp != "");

  assert(IsVideoTimeResolutionSupported(s->video_properties.time_resolution));
  double fps           = (double)s->video_properties.time_resolution.video_time_scale /
                              (double)s->video_properties.time_resolution.video_sample_duration;
  /* Get frame size in seconds */
  double input_seconds = (double)num_frames / fps;

  /* Sanity check */
  if(buffered_video_ptr               != NULL) {
    assert(buffered_audio_ptr         != NULL);
    assert(buffered_num_frames        != 0);
    assert(buffered_num_audio_samples != 0);
    assert(buffered_seconds           != 0);
  }
  if(buffered_video_ptr == NULL) {
    assert(buffered_sample_timestamp  == "");
    assert(buffered_sample_uniqueid   == "");
    assert(buffered_audio_ptr         == NULL);
    assert(buffered_num_frames        == 0);
    assert(buffered_num_audio_samples == 0);
    assert(buffered_seconds           == 0);
  }

  size_t video_frame_size  = GetFrameSize(s->video_properties);
  size_t audio_sample_size = GetAudioSampleSize(s->audio_properties);
  assert(video_frame_size != 0);
  assert(audio_sample_size != 0);

  size_t video_buffered_memory_size = video_frame_size * buffered_num_frames;
  size_t video_memory_extra         = video_frame_size * num_frames;
  size_t video_memory_reqd          = video_buffered_memory_size + video_memory_extra;

  size_t audio_buffered_memory_size = audio_sample_size * buffered_num_audio_samples;
  size_t audio_memory_extra         = audio_sample_size * num_audio_samples;
  size_t audio_memory_reqd          = audio_buffered_memory_size + audio_memory_extra;
  /* Check if we have buffered data already */
  bool already_buffered = false;
  if(buffered_video_ptr != NULL) { already_buffered = true; }

  /*TODO: Add proper exit condition to exit gracefully if we are not able to alloc
   * necessary memory.
   */
  if(already_buffered) {

    /* Expand video buffer */
    assert(buffered_video_ptr != NULL);
    buffered_video_ptr = (void *)realloc(buffered_video_ptr, video_memory_reqd);
    assert(buffered_video_ptr != NULL && "Cannot allocate memory");

    /* Expand audio buffer */
    assert(buffered_audio_ptr != NULL);
    buffered_audio_ptr = (void *)realloc(buffered_audio_ptr, audio_memory_reqd);
    assert(buffered_audio_ptr != NULL && "Cannot allocate memory");

  } else {

    /* Allocate video buffer */
    assert(buffered_video_ptr == NULL);
    buffered_video_ptr = (void *)malloc(video_memory_reqd);
    assert(buffered_video_ptr != NULL && "Cannot allocate memory");

    /* Allocate audio buffer */
    assert(buffered_audio_ptr == NULL);
    buffered_audio_ptr = (void *)malloc(audio_memory_reqd);
    assert(buffered_audio_ptr != NULL && "Cannot allocate memory");

  }

  /* Copy input video data to buffer */
  memcpy(((char *)buffered_video_ptr) + video_buffered_memory_size, video, video_memory_extra);

  /* Copy input audio data to buffer */
  memcpy(((char *)buffered_audio_ptr) + audio_buffered_memory_size, audio, audio_memory_extra);

  /* Update state variables */
  buffered_num_frames        += num_frames;
  buffered_num_audio_samples += num_audio_samples;
  buffered_seconds           += input_seconds;
  if(!already_buffered) {
    buffered_sample_timestamp = time_stamp;
    buffered_sample_uniqueid  = uniqueid;
  }
}

/*
 *  Description :
 *    A get method returning the value of 'is_created'
 */
bool LipSync::IsCreated() {
  return is_created;
}

/*
 * Descrition :
 *   A get method returning 's'.
 */
Stream *LipSync::GetStream() {
  return s;
}

/*
 * Description :
 *  Checks if a Stream object is associated with this LipSync object.
 *
 * Return True if a Stream object is associated.
 * Return False if a Stream object is not assoicated.
 */
bool LipSync::IsStreamAssociated() {
  return (!(s == NULL));
}

/*
 * Description :
 *   Join all callback threads and clear the callback_threads vector.
 */
void LipSync::JoinAllCallbackThreads() {

  bool threads_joined = LSJoinThreads(callback_threads);
  assert(threads_joined && "Cannot join threads");

  callback_threads.clear();
}

#undef CRITICAL_VOLUME
#undef CALC_INSYNC_BOUNDARY
